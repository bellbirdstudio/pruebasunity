﻿using UnityEngine;

public class PerliNoiseMapGenerator : MonoBehaviour {

    public int height = 512;
    public int width = 512;

    public void Start()
    {
        Terrain terrain = GetComponent<Terrain>();

        terrain.terrainData = GenerarTerrainData(terrain.terrainData);
    }

    private TerrainData GenerarTerrainData(TerrainData terrainData){

        terrainData.SetHeights(height, width,GenerarHeights());
        return terrainData;
    }

    private float[,] GenerarHeights(){

        float[,] heights = new float[height, width];

        for(int i =0; i < height; i++){
            for (int j = 0; j < width;j++){
                heights[i, j] = Mathf.PerlinNoise(i, j);
            }
        }

        return heights;
    }


}
