﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	[Serializable]
	public class TimeSignature
	{
		/// number of steps per measure
		public int mStepsPerMeasure { get; private set; }
		/// Notes per timestep
		public int[] mTimestepNum { get; private set; }
		/// inverted notes per timestep
		public int[] mTimestepNumInverse { get; private set; }

		/// sixteenth note;
		private int mSixteenth = 16;
		public int Sixteenth { get { return mSixteenth; } private set { mSixteenth = value; } }
		
		/// eighth note;
		private int mEighth = 8;
		public int Eighth { get { return mEighth; } private set { mEighth = value; } }
		
		/// quarter note;
		private int mQuarter = 4;
		public int Quarter { get { return mQuarter; } private set { mQuarter = value; } }
		
		/// half note;
		private int mHalf = 2;
		public int Half { get { return mHalf; } private set { mHalf = value; } }
		
		/// whole note;
		private int mWhole = 0;
		public int Whole { get { return mWhole; } private set { mWhole = value; } }

		///our currently set time signature.
		private eTimeSignature mSignature = eTimeSignature.FourFour;
		public eTimeSignature Signature { get { return mSignature; } set { SetTimeSignature (value); } }

		public void Init ()
		{
			mStepsPerMeasure = 16;
			mTimestepNum = new int[] { 16, 8, 4, 2, 1 };
			mTimestepNumInverse = new int[] { 1, 2, 4, 8, 16 };
		}

		/// Sets our time signature and adjusts values.
		public void SetTimeSignature (eTimeSignature signature)
		{
			mSignature = signature;
			/// Apologies for all the magic numbers. This is a bit of a hacky approach.
			/// trying to shoehorn everything to the same system.
			switch (mSignature)
			{
				case eTimeSignature.FourFour:
					{
						mStepsPerMeasure = 16;
						mTimestepNum = new int[] { 16, 8, 4, 2, 1 };
						mTimestepNumInverse = new int[] { 1, 2, 4, 8, 16 };
						Sixteenth = 16;
						Eighth = 8;
						Quarter = 4;
						Half = 2;
						Whole = 0;
						break;
					}
				case eTimeSignature.ThreeFour:
					{
						mStepsPerMeasure = 12;
						mTimestepNum = new int[] { 12, 6, 3, 3, 1 };
						mTimestepNumInverse = new int[] { 1, 3, 3, 6, 12 };
						Sixteenth = 12;
						Eighth = 6;
						Quarter = 3;
						Half = 3;
						Whole = 0;
						break;
					}
				case eTimeSignature.FiveFour:
					{
						mStepsPerMeasure = 20;
						mTimestepNum = new int[] { 20, 10, 5, 5, 1 };
						mTimestepNumInverse = new int[] { 1, 5, 5, 10, 20 };
						Sixteenth = 20;
						Eighth = 10;
						Quarter = 5;
						Half = 5;
						Whole = 0;
						break;
					}
			}

			MusicGenerator.Instance.ResetPlayer ();
		}
	}
}