﻿using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	/// data for our chord progressions
	public class ChordProgressionData
	{
		/// version number of this save.
		[SerializeField]
		private float mVersion = 0.0f;

		[Tooltip ("which steps of our current scale are excluded from our chord progression. see:https://en.wikipedia.org/wiki/Chord_progression.")]
		[SerializeField]
		public bool[] mExcludedProgSteps = new bool[] { false, false, false, false, false, false, false };

		[Tooltip ("influence of the liklihood of playing a tonic chord in our progression. This isn't straight-odds, and is finessed a little. see:  https://en.wikipedia.org/wiki/Tonic_(music)")]
		[Range (1, 100)]
		[SerializeField]
		private float mTonicInfluence = 50.0f;
		public float TonicInfluence { get { return mTonicInfluence; } set { mTonicInfluence = Mathf.Clamp (value, 0, 100); } }

		[Tooltip ("influence of the liklihood of playing a subdominant chord in our progression. This isn't straight-odds, and is finessed a little. see:  https://en.wikipedia.org/wiki/Subdominant")]
		[Range (0, 100)]
		[SerializeField]
		private float mSubdominantInfluence = 50.0f;
		public float SubdominantInfluence { get { return mSubdominantInfluence; } set { mSubdominantInfluence = Mathf.Clamp (value, 0, 100); } }

		[Tooltip ("influence of the liklihood of playing a dominant chord in our progression. This isn't straight-odds, and is finessed a little. see:  https://en.wikipedia.org/wiki/Dominant_(music)")]
		[Range (0, 100)]
		[SerializeField]
		private float mDominantInfluence = 50.0f;
		public float DominantInfluence { get { return mDominantInfluence; } set { mDominantInfluence = Mathf.Clamp (value, 0, 100); } }

		[Tooltip ("odds of our dominant chord being replaced by a flat-5 substitution. see: https://en.wikipedia.org/wiki/Tritone_substitution")]
		[Range (0, 100)]
		[SerializeField]
		private float mTritoneSubInfluence = 50.0f;
		public float TritoneSubInfluence { get { return mTritoneSubInfluence; } set { mTritoneSubInfluence = Mathf.Clamp (value, 0, 100); } }

		/// Saves to json
		public static void SaveData (string pathIN, ChordProgressionData data)
		{
			data.mVersion = MusicGenerator.Version;
			string save = JsonUtility.ToJson (data);
			File.WriteAllText (pathIN + "/ChordProgressionData.txt", save);
		}

		/// loads from json
		public static ChordProgressionData LoadData (string pathIN)
		{
			string data = "";
			string chordProgressionDataPath = pathIN + "/ChordProgressionData.txt";
			if (File.Exists (chordProgressionDataPath))
				data = File.ReadAllText (chordProgressionDataPath);

			ChordProgressionData saveOUT = JsonUtility.FromJson<ChordProgressionData> (data);
			if (saveOUT == null || saveOUT.mVersion != MusicGenerator.Version)
				return UpdateVersion (pathIN, saveOUT);

			return saveOUT;
		}

		/// Updates save type and variables for new generator versions.
		private static ChordProgressionData UpdateVersion (string pathIN, ChordProgressionData save)
		{
			if (save == null || save.mVersion == 0.0f)
			{
				string generatorPath = pathIN + "/generator.txt";
				/// we need to grab these from the generatorSave as the variables belonged to that in the last version
				if (File.Exists (generatorPath))
				{
					GeneratorSave generatorSave = JsonUtility.FromJson<GeneratorSave> (File.ReadAllText (generatorPath));
					save = new ChordProgressionData ();
					save.DominantInfluence = generatorSave.mDominantInfluence;
					save.mExcludedProgSteps = generatorSave.mExcludedProgSteps.ToArray ();
					save.SubdominantInfluence = generatorSave.mSubdominantInfluence;
					save.TonicInfluence = generatorSave.mTonicInfluence;
					save.TritoneSubInfluence = generatorSave.mTritoneSubInfluence;
				}
			}
			ChordProgressionData.SaveData (pathIN, save);
			return save;
		}
	}
}