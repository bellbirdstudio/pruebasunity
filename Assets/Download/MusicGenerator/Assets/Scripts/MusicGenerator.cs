﻿using System.Collections.Generic;
using System.Collections;
using System.IO;
using System.Text;
using System;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine;

namespace ProcGenMusic
{
	/// Music Generator:
	/// See the included setup documentation file.
	/// Handles the state logic and other top level functions for the entire player. Loading assets, etc.
	public class MusicGenerator : HelperSingleton<MusicGenerator>
	{
		/// Generator Version
		public const float Version = 1.1f;

		/// which platform we're on, this is detected in Awake().
		public string mPlatform { get; private set; }

		/// Music Generator state
		public eGeneratorState mState { get; private set; }

		/// Our generator data
		public MusicGeneratorData mGeneratorData = null;

		/// whether we're fading in or out
		public eVolumeState mVolumeState { get; private set; }

		/// Just stored strings for the mixer's volume.
		public static readonly string[] mVolNames = new string[] { "Volume0", "Volume1", "Volume2", "Volume3", "Volume4", "Volume5", "Volume6", "Volume7", "Volume8", "Volume9" };

		/// ref to our instrument set
		[SerializeField]
		public InstrumentSet mInstrumentSet = null;

		/// Our repeating measure logic
		[SerializeField]
		private Measure mRepeatingMeasure = null;

		/// Our regular measure logic
		[SerializeField]
		private Measure mRegularMeasure = null;

		/// Sets the instrument set;
		public void SetInstrumentSet (InstrumentSet setIN) { if (setIN != null) mInstrumentSet = setIN; }

		/// setter for fade rate. This must be positive value.
		public void SetVolFadeRate (float value) { mGeneratorData.mVolFadeRate = Math.Abs (value); }

		/// based on decibels. Needs to match vol slider on ui (if using ui). Edit at your own risk :)
		public const float mMaxVolume = 15;

		/// based on decibels. Needs to match vol slider on ui (if using ui). Edit at your own risk :)
		public const float mMinVolume = -100.0f;

		/// max number of notes per instrument
		public const int mMaxInstrumentNotes = 36;

		/// number of notes in an octave
		public const int mOctave = 12;

		/// number of audio sources we'll start with.
		public const float mNumStartingAudioSources = 10;

		/// default config loaded on start.
		[SerializeField]
		public string mDefaultConfig = "AAADefault";

		/// max number of steps per chord progression. Currently only support 4
		public const int mMaxFullstepsTaken = 4;

		/// is frankly how many will fit at the moment...Only made 10 mixer groups. 
		/// In theory there's no hard limit if you want to add mixer groups and expose their variables.
		static readonly public int mMaxInstruments = 10;

		/// our currently played chord progression
		private int[] mChordProgression;
		public int[] ChordProgression
		{
			get { return mChordProgression; } set
			{
				if (value.Length == 4)
					mChordProgression = value;
			}
		}

		/// whether we'll change key next measure
		private bool mKeyChangeNextMeasure = false;

		/// whether this group is currently playing
		public List<bool> mGroupIsPlaying { get; private set; }

		/// chord progression logic
		public ChordProgressions mChordProgressions { get; private set; }

		/// list of audio sources :P
		private List<AudioSource> mAudioSources = new List<AudioSource> ();

		/// our audio mixer
		public AudioMixer mMixer { get; private set; }

		/// Reference to MusicFileConfig
		public MusicFileConfig mMusicFileConfig { get; private set; }

		/// loaded instrument paths for the current configuration
		private List<string> mLoadedInstrumentNames = new List<string> ();
		public List<string> LoadedInstrumentNames { get { return mLoadedInstrumentNames; } private set { } }

		/// Set in the editor. Possible instrument paths
		[SerializeField]
		private List<string> mBaseInstrumentPaths = new List<string> ();
		public List<string> BaseInstrumentPaths { get { return mBaseInstrumentPaths; } private set { mBaseInstrumentPaths = value; } }

		/// multidimensional list of clips. top index is instrument
		private List<List<List<AudioClip>> > mAllClips = new List<List<List<AudioClip>> > ();
		public List<List<List<AudioClip>> > AllClips { get { return mAllClips; } private set { mAllClips = value; } }

		/// keeps the asset bundles from being unloaded which causes an FMOD error if you try to edit the
		/// audio mixer in the editor while it's playing. Leave false unless you're using the audio mixer live in unity'd editor.
		[SerializeField]
		private bool mEnableLiveMixerEditing = false;

		/// Whether we'll use async loading. Cannot set while initializing already.
		[SerializeField]
		private bool mUseAsyncLoading = false;
		public bool UseAsyncLoading
		{
			get { return mUseAsyncLoading; }
			set { mUseAsyncLoading = mState != eGeneratorState.initializing ? value : mUseAsyncLoading; }
		}

		////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////
		/// public functions:
		////////////////////////////////////////////////////////////

		/// Loads a new configuration (song) to play.
		public void LoadConfiguration (string configIN, eGeneratorState continueState = eGeneratorState.ready)
		{
			if (mState == eGeneratorState.initializing)
				return;

			if (mUseAsyncLoading)
				StartCoroutine (AsyncLoadConfiguration (configIN, continueState));
			else
				NonAsyncLoadConfiguration (configIN, continueState);
		}

		/// Fades out the music before async loading a new configuration and fading back in.
		public IEnumerator FadeLoadConfiguration (string configIN)
		{
			mGeneratorData.mStateTimer = 0.0f;
			VolumeFadeOut ();
			float maxWaitTime = 10.0f;
			yield return new WaitUntil (() => mVolumeState == eVolumeState.fadedOutIdle || mGeneratorData.mStateTimer > maxWaitTime);

			Stop ();
			bool finished = false;
			StartCoroutine (mMusicFileConfig.AsyncLoadConfig (configIN, ((x) => { finished = x; })));
			yield return new WaitUntil (() => finished);
			mVolumeState = eVolumeState.idle;
			SetState (eGeneratorState.playing);
			yield return null;
		}

		/// returns mInstrumentSet.instruments
		public List<Instrument> GetInstruments ()
		{
			return mInstrumentSet.mInstruments;
		}

		/// pauses the main music generator:
		public void Pause ()
		{
			PauseGenerator.Invoke ();
			if (mState != eGeneratorState.initializing)
				SetState ((mState < eGeneratorState.editing) ? eGeneratorState.paused : eGeneratorState.editorPaused);
		}

		/// plays the main music generator:
		public void Play ()
		{
			PlayGenerator.Invoke ();
			if (mState != eGeneratorState.initializing)
				SetState ((mState < eGeneratorState.editing) ? eGeneratorState.playing : eGeneratorState.editorPlaying);
		}

		/// stops the main music generator:
		public void Stop ()
		{
			StopGenerator.Invoke ();
			if (mState != eGeneratorState.initializing)
				SetState ((mState < eGeneratorState.editing) ? eGeneratorState.stopped : eGeneratorState.editorStopped);
		}

		/// Set the music generator state:
		public void SetState (eGeneratorState stateIN)
		{
			mGeneratorData.mStateTimer = 0.0f;
			mState = stateIN;

			StateSet.Invoke (mState);

			switch (mState)
			{
				case eGeneratorState.stopped:
				case eGeneratorState.editorStopped:
					{
						ResetPlayer ();
						break;
					}
				case eGeneratorState.initializing:
				case eGeneratorState.ready:
				case eGeneratorState.playing:
				case eGeneratorState.repeating:
				case eGeneratorState.paused:
				case eGeneratorState.editing:
				case eGeneratorState.editorPaused:
				case eGeneratorState.editorPlaying:
				default:
					break;
			}
		}

		/// fades volume out
		public void VolumeFadeOut ()
		{
			mVolumeState = eVolumeState.fadingOut;
		}

		/// fades volume in
		public void VolumeFadeIn ()
		{
			mVolumeState = eVolumeState.fadingIn;
		}

		/// Sets the audio source volume for mAudioSource[indexIN]. This is different from the instrument volume which controls the clips
		/// played by an instrument, in that it controls the attenuation of the audioSource itself. If everything
		/// feels too quiet, this may be a good place to increase volume.
		public void SetAudioSourceVolume (int indexIN, float volumeIN, InstrumentSet set = null)
		{
			if (set == null)
				set = mInstrumentSet;
			mMixer.SetFloat ("Volume" + indexIN.ToString (), volumeIN);
			set.mInstruments[indexIN].mData.AudioSourceVolume = volumeIN;
		}

		/// plays an audio clip:
		/// Look for an available clip that's not playing anything, creates a new one if necessary
		/// resets its properties  (volume, pan, etc) to match our new clip.
		public void PlayAudioClip (InstrumentSet set, AudioClip clipIN, float volumeIN, int indexIN)
		{
			bool foundUnplayed = false;
			for (int i = 0; i < mAudioSources.Count; i++)
			{
				if (!mAudioSources[i].isPlaying)
				{
					mAudioSources[i].panStereo = set.mInstruments[indexIN].mData.mStereoPan;
					mAudioSources[i].loop = false;
					mAudioSources[i].outputAudioMixerGroup = mMixer.FindMatchingGroups (indexIN.ToString ()) [0];
					mAudioSources[i].volume = volumeIN;
					mAudioSources[i].clip = clipIN;
					mAudioSources[i].Play ();
					foundUnplayed = true;
					return;
				}
			}

			if (!foundUnplayed) //make a new audio souce.
			{
				mAudioSources.Add (Camera.main.gameObject.AddComponent<AudioSource> ());
				AudioSource audioSource = mAudioSources[mAudioSources.Count - 1];
				audioSource.panStereo = set.mInstruments[indexIN].mData.mStereoPan;
				audioSource.outputAudioMixerGroup = mMixer.FindMatchingGroups (indexIN.ToString ()) [0];
				audioSource.volume = volumeIN;
				audioSource.loop = false;
				audioSource.clip = clipIN;
				//audioSource.spatialBlend = 0;
				audioSource.Play ();
			}
		}

		/// resets all player settings.
		/// reset player is called on things like loading new configurations, loading new levels, etc.
		/// sets all timing values and other settings back to the start
		public void ResetPlayer ()
		{
			if (mState <= eGeneratorState.initializing || mChordProgressions == null)
				return;

			mInstrumentSet.Reset ();
			mRegularMeasure.ResetMeasure (mInstrumentSet, null, true);
			mRepeatingMeasure.ResetMeasure (mInstrumentSet, null, true);
			for (int i = 0; i < 4; i++)
				mGroupIsPlaying[i] = (i == 0) ? true : false;

			if (mState < eGeneratorState.editing)
				ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, mGeneratorData.mKeySteps);

			NormalMeasureExited.Invoke ();
			PlayerReset.Invoke ();
			mInstrumentSet.ResetRepeatCount ();
			mInstrumentSet.ResetProgressionSteps ();
		}

		/// Adds an instrument to our list. sets its instrument index
		public void AddInstrument (InstrumentSet setIN)
		{
			setIN.mInstruments.Add (new Instrument ());
			setIN.mInstruments[setIN.mInstruments.Count - 1].Init (setIN.mInstruments.Count - 1);
		}

		/// Deletes all instruments:
		public void ClearInstruments (InstrumentSet setIN)
		{
			InstrumentsCleared.Invoke ();
			if (setIN.mInstruments.Count == 0)
				return;

			for (int i = (int) setIN.mInstruments.Count - 1; i >= 0; i--)
			{
				RemoveInstrument ((int) setIN.mInstruments[i].InstrumentIndex, setIN);
			}
			setIN.mInstruments.Clear ();
		}

		/// Removes the instrument from our list. Fixes instrument indices.
		public void RemoveInstrument (int indexIN, InstrumentSet set)
		{
			int typeIndex = (int) set.mInstruments[indexIN].InstrumentTypeIndex;
			set.mInstruments[indexIN] = null;
			set.mInstruments.RemoveAt (indexIN);

			for (int i = 0; i < set.mInstruments.Count; i++)
				set.mInstruments[(int) i].InstrumentIndex = i;

			RemoveBaseClip (typeIndex, set);
		}

		/// Removes a base clip if there are no instruments using it.
		public void RemoveBaseClip (int typeIndex, InstrumentSet set)
		{
			bool isUsed = false;
			for (int i = 0; i < set.mInstruments.Count; i++)
			{
				if (set.mInstruments[i].InstrumentTypeIndex == typeIndex)
					isUsed = true;
			}
			if (!isUsed)
			{
				AllClips.RemoveAt (typeIndex);
				mLoadedInstrumentNames.RemoveAt (typeIndex);
			}
			CleanUpInstrumentTypeIndices (set);
		}

		/// Re-fixes the instrument types if something's been removed. There's probably a better way to do this.
		public void CleanUpInstrumentTypeIndices (InstrumentSet set)
		{
			for (int i = 0; i < set.mInstruments.Count; i++)
			{
				set.mInstruments[i].InstrumentTypeIndex = LoadedInstrumentNames.IndexOf (set.mInstruments[i].mData.InstrumentType);
			}
		}

		/// Sets the volume.
		public void SetVolume (float volIN)
		{
			if (mVolumeState == eVolumeState.idle)
			{
				mGeneratorData.mMasterVolume = volIN;
				mMixer.SetFloat (MusicGeneratorData.mMasterVolName, mGeneratorData.mMasterVolume);
			}
		}

		/// Sets a global effect on the mixer. Can also edit manually on the master channel of the 'GeneratorMixer' in your scene.
		/// PLEASE USE WITH CAUTION!!! :) There's no idiot proofing on these values' effect on your speakers, game, pet's ears, your ears, etc. You could wake an Old One for all I know :P
		/// You can use the executable UI included with the asset to reset any of these to their defaults (click the reset button next to the slider)
		/// the mixer's current effects names and unity's min/max ranges :
		/// See: The Pair_String_Float variables above for settings these:
		/// "MasterDistortion"  0 : 1
		/// "MasterCenterFrequency" 20 : 22000
		/// "MasterOctaveRange" .2 : 5
		/// "MasterRoomSize" -10000 : 0
		///	"MasterReverbDecay" .1 : 5
		///	"MasterFrequencyGain" .05 : 3
		/// "MasterLowpassCutoffFreq" 10 : 22000
		/// "MasterLowpassResonance" 1 : 10
		/// "MasterHighpassCutoffFreq" 10 : 22000
		/// "MasterHighpassResonance" 1 : 10
		/// "MasterEchoDelay" 1 : 5000
		/// "MasterEchoDecay" .01 : 1
		/// "MasterEchoDry" 0 : 100
		/// "MasterEchoWet" 0 : 100
		/// "MasterNumEchoChannels" 0 : 16
		/// "MasterReverb" -10000 : 2000
		public void SetGlobalEffect (Pair_String_Float valueIN)
		{
			mMixer.SetFloat (valueIN.First, valueIN.Second);

		}

		/// Returns the shortest rhythm timestep
		public eTimestep GetShortestRhythmTimestep ()
		{
			List<Instrument> instruments = mInstrumentSet.mInstruments;
			eTimestep shortestTime = eTimestep.whole;
			for (int i = 0; i < instruments.Count; i++)
			{
				if (instruments[i].mData.mSuccessionType != eSuccessionType.lead)
					shortestTime = instruments[i].mData.mTimeStep < shortestTime ? instruments[i].mData.mTimeStep : shortestTime;
			}
			return shortestTime;
		}

		////////////////////////////////////////////////////////////
		/// private utility functions:  Edit at your own risk :)
		////////////////////////////////////////////////////////////
		public override void Awake ()
		{
			mPlatform = "/Windows";
			if (Application.platform == RuntimePlatform.LinuxPlayer || Application.platform == RuntimePlatform.LinuxEditor)
				mPlatform = "/Linux";
			if (Application.platform == RuntimePlatform.OSXPlayer || Application.platform == RuntimePlatform.OSXEditor)
				mPlatform = "/Mac";	

			ChordProgression = new int[4] { 1, 4, 4, 5 };
			mChordProgressions = new ChordProgressions ();

			DontDestroyOnLoad (this.gameObject);

			mMusicFileConfig = gameObject.AddComponent<MusicFileConfig> ();
			mGeneratorData = new MusicGeneratorData ();

			//initialization of privately set values:
			mGroupIsPlaying = new List<bool> () { true, false, false, false };
			mState = eGeneratorState.loading;
			mVolumeState = eVolumeState.idle;

			if (mInstrumentSet == null)
				SetInstrumentSet (gameObject.AddComponent<InstrumentSet> ());
			if (mRegularMeasure == null)
				mRegularMeasure = gameObject.AddComponent<RegularMeasure> ();
			if (mRepeatingMeasure == null)
				mRepeatingMeasure = gameObject.AddComponent<RepeatMeasure> ();

			SetInstrumentSet (mInstrumentSet);

			///load our mixer:
			string mixerPath = Application.streamingAssetsPath + "/MusicGenerator" + mPlatform + "/GeneratorMixer";
			if (!System.IO.File.Exists (mixerPath))
			{
				Debug.Log ("Audio Mixer asset bundle does not exist.");
				throw new System.ArgumentNullException ("Audio Mixer base file does not exist.");
			}
			else
			{
				var loadedAssetBundle = AssetBundle.LoadFromFile (mixerPath);
				if (loadedAssetBundle != null)
				{
					mMixer = loadedAssetBundle.LoadAsset<AudioMixer> ("GeneratorMixer");
					if (mMixer == null)
						throw new System.ArgumentNullException ("GeneratorMixer base file was not sucessfully loaded");

					loadedAssetBundle.Unload (false);
				}
			}

			// These are generated as needed, but we'll certainly need some on start
			// this just keeps it from trying to add a dozen audio sources after things have already started playing.
			for (int i = 0; i < mNumStartingAudioSources; i++)
				mAudioSources.Add (Camera.main.gameObject.AddComponent<AudioSource> ());
		}

		///Loads an instrument set configuration:
		public void NonAsyncLoadConfiguration (string configIN, eGeneratorState continueState = eGeneratorState.ready)
		{
			if (mState > eGeneratorState.initializing && mChordProgressions != null)
				Stop ();
			ResetPlayer ();
			SetState (eGeneratorState.initializing);
			if (mMusicFileConfig != null)
				mMusicFileConfig.LoadConfig (configIN);
			else
				throw new ArgumentNullException ("configuration class doesn't exist or was not loaded yes");
			ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, 0);
			SetState (continueState);
		}

		/// Async Loads an instrument set configuration:
		public IEnumerator AsyncLoadConfiguration (string configIN, eGeneratorState continueState = eGeneratorState.ready)
		{
			ResetPlayer ();
			SetState (eGeneratorState.initializing);
			bool finished = false;
			StartCoroutine (mMusicFileConfig.AsyncLoadConfig (configIN, ((x) => { finished = x; })));
			yield return new WaitUntil (() => finished);
			yield return null;
			ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, 0);
			SetState (continueState);
			yield return null;
		}

		/// Asynchronously Loads the instrument clips from the asset bundles into our mAllClips array
		public IEnumerator AsyncLoadBaseClips (string instrumentType, System.Action<int> callback)
		{
			if (mLoadedInstrumentNames.Contains (instrumentType))
			{
				callback (mLoadedInstrumentNames.IndexOf (instrumentType));
				yield break;
			}

			mLoadedInstrumentNames.Add (instrumentType);

			string path = Application.streamingAssetsPath + "/MusicGenerator" + mPlatform + "/" + instrumentType + "_1";

			// Check for instrument sub-types.
			if (File.Exists (path))
			{
				AllClips.Add (new List<List<AudioClip>> ());
				yield return null;
				for (int i = 1; i < mMaxInstruments + 1; i++)
				{
					if (File.Exists (Application.streamingAssetsPath + "/MusicGenerator" + mPlatform + "/" + instrumentType + "_" + i.ToString ()))
					{
						InstrumentPrefabList list = null;
						StartCoroutine (AsyncLoadInstrumentPrefabList (instrumentType + "_" + i.ToString (), ((x) => { list = x; })));
						yield return new WaitUntil (() => list != null);

						AllClips[AllClips.Count - 1].Add (new List<AudioClip> ());
						yield return null;
						for (int x = 0; x < list.mAudioSources.Length; x++)
						{
							if (list.mAudioSources[x] != null)
							{
								AllClips[AllClips.Count - 1][i - 1].Add (list.mAudioSources[x].clip);
								yield return null;
							}
							else break;
						}
						yield return null;
					}
					else break;
				}
				callback (AllClips.Count - 1);
				yield return null;
			}
			else //load normal instrument without sub folders.
			{
				AllClips.Add (new List<List<AudioClip>> ());
				int index = AllClips.Count - 1;
				AllClips[index].Add (new List<AudioClip> ());
				InstrumentPrefabList list = null;
				StartCoroutine (AsyncLoadInstrumentPrefabList (instrumentType, ((x) => { list = x; })));
				yield return new WaitUntil (() => list != null);

				for (int i = 0; i < list.mAudioSources.Length; i++)
				{
					if (list.mAudioSources[i] != null)
					{
						AllClips[index][AllClips[index].Count - 1].Add (list.mAudioSources[i].clip);
						yield return null;
					}
					else break;
				}

				callback (AllClips.Count - 1);
				yield return null;
			}
		}

		/// async Loads our audio clip from its asset bundle.
		/// this is intentially really slow, so as to interfere as little as possible with the framerate.
		private IEnumerator AsyncLoadInstrumentPrefabList (string pathName, System.Action<InstrumentPrefabList> callback)
		{
			string path = Path.Combine (Application.streamingAssetsPath + "/MusicGenerator" + mPlatform, pathName);

			if (!File.Exists (path))
			{
				Debug.Log ("file at " + path + " does not exist");
				throw new ArgumentNullException ("file at " + path + " does not exist");
			}

			var bundleRequest = AssetBundle.LoadFromFileAsync (path);
			yield return new WaitUntil (() => bundleRequest.isDone);
			yield return null;
			var myLoadedAssetBundle = bundleRequest.assetBundle;
			if (myLoadedAssetBundle != null)
			{
				var assetLoadRequest = myLoadedAssetBundle.LoadAssetAsync<GameObject> (pathName);
				yield return new WaitUntil (() => assetLoadRequest.isDone);
				yield return null;
				GameObject go = assetLoadRequest.asset as GameObject;

				if (!mEnableLiveMixerEditing)
					myLoadedAssetBundle.Unload (false);
				yield return null;
				callback (go.GetComponent<InstrumentPrefabList> ());
			}
			else
			{
				throw new ArgumentNullException ("asset bundle for " + pathName + " could not be loaded or does not exist.");
			}
			yield return null;
		}

		/// non-async. Loads the instrument clips from file into our mAllClips array
		/// Use if you need / want to preload clips when loading other assets, instead of on the fly.
		public int LoadBaseClips (string instrumentType)
		{
			if (mLoadedInstrumentNames.Contains (instrumentType))
				return mLoadedInstrumentNames.IndexOf (instrumentType);

			mLoadedInstrumentNames.Add (instrumentType);

			string path = Application.streamingAssetsPath + "/MusicGenerator" + mPlatform + "/" + instrumentType + "_1";

			// Here we test for sub folder for the instrument and load any additional variations:
			if (File.Exists (path))
			{
				AllClips.Add (new List<List<AudioClip>> ());
				for (int i = 1; i < mMaxInstruments + 1; i++)
				{
					if (File.Exists (Application.streamingAssetsPath + "/MusicGenerator" + mPlatform + "/" + instrumentType + "_" + i.ToString ()))
					{
						InstrumentPrefabList list = LoadInstrumentPrefabList (instrumentType + "_" + i.ToString ());

						if (list == null)
							throw new ArgumentNullException ("prefab list for " + instrumentType + " does not exist");

						AllClips[AllClips.Count - 1].Add (new List<AudioClip> ());

						for (int x = 0; x < list.mAudioSources.Length; x++) //musician who named them started at 1, just haven't fixed. :P
						{
							if (list.mAudioSources[x] != null)
								AllClips[AllClips.Count - 1][i - 1].Add (list.mAudioSources[x].clip);
						}
					}
					else break;
				}
				return AllClips.Count - 1;
			}
			else // otherwise load normal instrument without sub folders.
			{
				AllClips.Add (new List<List<AudioClip>> ());
				int index = AllClips.Count - 1;
				AllClips[index].Add (new List<AudioClip> ());
				InstrumentPrefabList list = LoadInstrumentPrefabList (instrumentType);
				if (list == null)
					throw new ArgumentNullException ("prefab list for " + instrumentType + " does not exist");

				for (int i = 0; i < list.mAudioSources.Length; i++)
				{
					if (list.mAudioSources[i] != null)
						AllClips[index][AllClips[index].Count - 1].Add (list.mAudioSources[i].clip);
				}

				return AllClips.Count - 1;;
			}
		}

		/// non-async. Loads our audio clip from its asset bundle.
		private InstrumentPrefabList LoadInstrumentPrefabList (string pathName)
		{
			string path = Path.Combine (Application.streamingAssetsPath + "/MusicGenerator" + mPlatform, pathName);

			var myLoadedAssetBundle = AssetBundle.LoadFromFile (path);

			if (myLoadedAssetBundle != null)
			{
				GameObject go = myLoadedAssetBundle.LoadAsset<GameObject> (pathName);
				if (!mEnableLiveMixerEditing)
					myLoadedAssetBundle.Unload (false);
				return go.GetComponent<InstrumentPrefabList> ();
			}
			else
			{
				throw new ArgumentNullException ("asset bundle for " + pathName + " could not be loaded or does not exist.");
			}
		}

		/// Loads the initial configuration.
		void Start ()
		{
			Started.Invoke ();
			
			if (!OnHasVisiblePlayer ()) //without the UI, we load manually, as the UI panel is not going to do it, and we don't need ui initialized.
			{
				if (mUseAsyncLoading)
					StartCoroutine (AsyncLoadConfiguration (mDefaultConfig));
				else
					LoadConfiguration (mDefaultConfig);
			}
			else
				ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, 0);

			/// Music generator is now ready to have functions called upon it.
			Ready.Invoke();
		}

		/// Generates new chord progression:
		private void GenNewChordProgression ()
		{
			ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, mGeneratorData.mKeySteps);
			ProgressionGenerated.Invoke ();
		}

		/// State update:
		void Update () { UpdateState (Time.deltaTime); }
		private void UpdateState (float deltaT)
		{
			mGeneratorData.mStateTimer += deltaT;

			if (mState != eGeneratorState.initializing &&
				mState != eGeneratorState.editorInitializing &&
				mState != eGeneratorState.loading)
				UpdateLiveSettings ();

			/// just idiot-proofing. I don't want blame for anyone speakers :P
			/// Feel free to adjust min/max values to your needs.
			mGeneratorData.mMasterVolume = mGeneratorData.mMasterVolume <= mMaxVolume ? mGeneratorData.mMasterVolume : mMaxVolume;
			mGeneratorData.mMasterVolume = mGeneratorData.mMasterVolume >= mMinVolume ? mGeneratorData.mMasterVolume : mMinVolume;

			switch (mState)
			{
				case eGeneratorState.ready:
					break;
				case eGeneratorState.playing:
					{
						mRegularMeasure.PlayMeasure (mInstrumentSet, CheckKeyChange, SetThemeRepeat, GenerateNewProgression);
						break;
					}
				case eGeneratorState.repeating:
					{
						mRepeatingMeasure.PlayMeasure (mInstrumentSet);
						break;
					}
				case eGeneratorState.editorPlaying:
					{
						PlayMeasureEditorClip ();
						break;
					}
					/// nothing to do:
				case eGeneratorState.initializing:
				case eGeneratorState.paused:
				case eGeneratorState.stopped:
					/// these are handled only by the ui measure editor
				case eGeneratorState.editing:
				case eGeneratorState.editorPaused:
				case eGeneratorState.editorStopped:
				default:
					break;
			}

			/// handles the volume fade in / fade out:
			switch (mVolumeState)
			{
				case eVolumeState.fadingIn:
				case eVolumeState.fadingOut:
					FadeVolume (deltaT);
					break;
				case eVolumeState.fadedOutIdle:
				case eVolumeState.idle:
				default:
					break;
			}
			StateUpdated.Invoke (mState);
		}

		/// fades the volume:
		private void FadeVolume (float deltaT)
		{
			float currentVol;
			mMixer.GetFloat ("MasterVol", out currentVol);

			switch (mVolumeState)
			{
				case eVolumeState.fadingIn:
					{
						if (currentVol <= mGeneratorData.mMasterVolume - (mGeneratorData.mVolFadeRate * deltaT))
							currentVol += mGeneratorData.mVolFadeRate * deltaT;
						else
						{
							currentVol = mGeneratorData.mMasterVolume;
							mVolumeState = eVolumeState.idle;
						}
						break;
					}
				case eVolumeState.fadingOut:
					{
						if (currentVol > mMinVolume + (mGeneratorData.mVolFadeRate * deltaT))
							currentVol -= mGeneratorData.mVolFadeRate * deltaT;
						else
						{
							currentVol = mMinVolume;
							mVolumeState = eVolumeState.fadedOutIdle;
						}
						break;
					}
				default:
					break;
			}

			mMixer.SetFloat ("MasterVol", currentVol);

			VolumeFaded.Invoke (currentVol);
		}

		// handles necessary updates when music is not stopped or paused.
		private void UpdateLiveSettings ()
		{
			UpdateEffects ();
		}

		/// Updates mixer effects.
		private void UpdateEffects ()
		{
			if (mVolumeState == eVolumeState.idle)
				SetVolume (mGeneratorData.mMasterVolume);
			mMixer.SetFloat (mGeneratorData.mDistortion.First, mGeneratorData.mDistortion.Second);
			mMixer.SetFloat (mGeneratorData.mCenterFreq.First, mGeneratorData.mCenterFreq.Second);
			mMixer.SetFloat (mGeneratorData.mOctaveRange.First, mGeneratorData.mOctaveRange.Second);
			mMixer.SetFloat (mGeneratorData.mFreqGain.First, mGeneratorData.mFreqGain.Second);
			mMixer.SetFloat (mGeneratorData.mLowpassCutoffFreq.First, mGeneratorData.mLowpassCutoffFreq.Second);
			mMixer.SetFloat (mGeneratorData.mLowpassResonance.First, mGeneratorData.mLowpassResonance.Second);
			mMixer.SetFloat (mGeneratorData.mHighpassCutoffFreq.First, mGeneratorData.mHighpassCutoffFreq.Second);
			mMixer.SetFloat (mGeneratorData.mHighpassResonance.First, mGeneratorData.mHighpassResonance.Second);
			mMixer.SetFloat (mGeneratorData.mEchoDelay.First, mGeneratorData.mEchoDelay.Second);
			mMixer.SetFloat (mGeneratorData.mEchoDecay.First, mGeneratorData.mEchoDecay.Second);
			mMixer.SetFloat (mGeneratorData.mEchoDry.First, mGeneratorData.mEchoDry.Second);
			mMixer.SetFloat (mGeneratorData.mEchoWet.First, mGeneratorData.mEchoWet.Second);
			mMixer.SetFloat (mGeneratorData.mNumEchoChannels.First, mGeneratorData.mNumEchoChannels.Second);
			mMixer.SetFloat (mGeneratorData.mReverb.First, mGeneratorData.mReverb.Second);
			mMixer.SetFloat (mGeneratorData.mRoomSize.First, mGeneratorData.mRoomSize.Second);
			mMixer.SetFloat (mGeneratorData.mReverbDecay.First, mGeneratorData.mReverbDecay.Second);

			for (int i = 0; i < mInstrumentSet.mInstruments.Count; i++)
				mMixer.SetFloat (mVolNames[i], mInstrumentSet.mInstruments[i].mData.AudioSourceVolume);
		}

		/// Checks for a keychange and starts setup if needed.
		private void CheckKeyChange ()
		{
			if (mInstrumentSet.ProgressionStepsTaken == 1)
				KeyChangeSetup ();
		}

		/// Generates a new chord progression
		private void GenerateNewProgression ()
		{
			if (mInstrumentSet.ProgressionStepsTaken >= mMaxFullstepsTaken - 1)
			{
				SetKeyChange ();

				if (UnityEngine.Random.Range (0, 100) < mGeneratorData.mProgressionChangeOdds || mKeyChangeNextMeasure)
					ChordProgression = mChordProgressions.GenerateProgression (mGeneratorData.mMode, mGeneratorData.mScale, mGeneratorData.mKeySteps);
			}
		}

		/// Sets theme / repeat variables.
		private void SetThemeRepeat ()
		{
			if (mInstrumentSet.mRepeatCount >= mInstrumentSet.mData.RepeatMeasuresNum)
			{
				if (mGeneratorData.mThemeRepeatOptions > (int) eThemeRepeatOptions.eNone && mState == eGeneratorState.playing && mInstrumentSet.mRepeatCount >= mInstrumentSet.mData.RepeatMeasuresNum)
					SetState (eGeneratorState.repeating);

				// set our theme notes if we're going to use them.
				bool newInstrumentDetected = false;
				for (int i = 0; i < mInstrumentSet.mInstruments.Count; i++)
				{
					if (mInstrumentSet.mInstruments[i].mNeedsTheme)
						newInstrumentDetected = true;
				}
				if (UnityEngine.Random.Range (0, 100.0f) < mGeneratorData.mSetThemeOdds || newInstrumentDetected)
				{
					if (mInstrumentSet.mRepeatCount >= mInstrumentSet.mData.RepeatMeasuresNum)
					{
						for (int i = 0; i < mInstrumentSet.mInstruments.Count; i++)
							mInstrumentSet.mInstruments[i].SetThemeNotes ();
					}
				}
			}
			NormalMeasureExited.Invoke ();
		}

		/// sets up whether we'll change keys next measure: 
		private void KeyChangeSetup ()
		{
			if (mKeyChangeNextMeasure)
			{
				mGeneratorData.mKey += mGeneratorData.mKeySteps;
				mGeneratorData.mKey = (int) mGeneratorData.mKey >= mOctave ? (eKey) ((mOctave - (int) mGeneratorData.mKey) * -1) : (eKey) (mOctave + mGeneratorData.mKey);

				mKeyChangeNextMeasure = false;
				KeyChanged.Invoke ((int) mGeneratorData.mKey);
			}
		}

		/// changes the key for the current instrument set:
		/// alters the current chord progression to allow a smooth transition to 
		/// the new key
		private void SetKeyChange ()
		{
			if (UnityEngine.Random.Range (0.0f, 100.0f) < mGeneratorData.mKeyChangeOdds)
			{
				mKeyChangeNextMeasure = true;
				mGeneratorData.mKeySteps = (UnityEngine.Random.Range (0, 100) < mGeneratorData.mKeyChangeAscendDescend) ? -Instrument.mScaleLength : Instrument.mScaleLength;
			}
			else
				mGeneratorData.mKeySteps = 0;
		}

		/// UI measure editor version only (for normal use, play clips through the singleClip state functions: )
		private void PlayMeasureEditorClip ()
		{
			EditorClipPlayed.Invoke ();
		}

		////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////
		/// Events: Most of these are for the UI and can be safely ignored, unless you need to subscribe for your app uses.
		/// feel free to subscribe to them or call them, if your use of the generator requires knowing any of these events.
		////////////////////////////////////////////////////////////
		/// On manager start event:
		public UnityEvent Started = new UnityEvent ();

		/// Music generator is fully initialized and ready
		public UnityEvent Ready = new UnityEvent();
		
		/// Event for Generator state set:
		public class StateEvent : UnityEvent<eGeneratorState> { }
		public StateEvent StateSet = new StateEvent ();

		/// Event for state Update()
		public StateEvent StateUpdated = new StateEvent ();

		/// Event Handler for UI manager detection:
		/// This is a bit hacky. Please don't listen/return anything for this.
		/// It's used to detect UI states without being coupled to them.
		public delegate bool HasVisiblePlayerEventHandler (object source, EventArgs args);
		public event HasVisiblePlayerEventHandler HasVisiblePlayer;
		protected virtual bool OnHasVisiblePlayer ()
		{
			if (HasVisiblePlayer != null)
				return HasVisiblePlayer (this, EventArgs.Empty);
			return false;
		}

		/// Event Handler for fading volume
		public class FloatEvent : UnityEvent<float> { };
		public FloatEvent VolumeFaded = new FloatEvent ();

		/// Event Handler for Generates a chord progression:
		public UnityEvent ProgressionGenerated = new UnityEvent ();

		/// Event handler for clear instruments:
		public UnityEvent InstrumentsCleared = new UnityEvent ();

		/// Event Handler for exiting normal measure
		public UnityEvent NormalMeasureExited = new UnityEvent ();

		/// Event Handler for impending key change:
		public class IntEvent : UnityEvent<int> { }
		public IntEvent KeyChanged = new IntEvent ();

		// Event for exiting the repeating measure.
		public StateEvent RepeatedMeasureExited = new StateEvent ();

		/// Set barline color event
		public class BarlineEvent : UnityEvent<int, bool> { }
		public BarlineEvent BarlineColorSet = new BarlineEvent ();

		/// Editor clip played event:
		public UnityEvent EditorClipPlayed = new UnityEvent ();

		/// Event Handler for a the measure editor opening
		/// This is a bit hacky. Please don't listen/return anything here.
		/// It's used to detect UI states without being coupled to them.
		public delegate bool UIPlayerIsEditingEventHandler (object source, EventArgs args);
		public event UIPlayerIsEditingEventHandler UIPlayerIsEditing;
		public bool OnUIPlayerIsEditing ()
		{
			if (UIPlayerIsEditing != null)
				return UIPlayerIsEditing (this, EventArgs.Empty);

			return false;
		}

		/// Events for repeating notes:
		public class RepeatNoteEvent : UnityEvent<RepeatNoteArgs> { }
		public RepeatNoteEvent RepeatNotePlayed = new RepeatNoteEvent ();

		/// Event for staff player:
		public class IntIntEvent : UnityEvent<int, int> { }
		public IntIntEvent UIStaffNotePlayed = new IntIntEvent ();

		/// Event for staff player strummed:
		public IntIntEvent UIStaffNoteStrummed = new IntIntEvent ();

		/// Event Single clip being loaded:
		public class ClipEvent : UnityEvent<ClipSave> { }
		public ClipEvent ClipLoaded = new ClipEvent ();

		/// Event for player being reset:
		public UnityEvent PlayerReset = new UnityEvent ();

		/// Event for play()
		public UnityEvent PlayGenerator = new UnityEvent ();

		/// Event for stop()
		public UnityEvent StopGenerator = new UnityEvent ();

		/// Event for pause()
		public UnityEvent PauseGenerator = new UnityEvent ();

		/////////////////////////////////
		/// Save / Load /////////////////
		/////////////////////////////////

		/// Sets variables from save file.
		public void LoadGeneratorSave (MusicGeneratorData data)
		{
			mGeneratorData = data;

			/// best to update our mixer now. it will get updated anyway, but the UI needs it updated sooner.
			SetGlobalEffect (mGeneratorData.mDistortion);
			SetGlobalEffect (mGeneratorData.mCenterFreq);
			SetGlobalEffect (mGeneratorData.mOctaveRange);
			SetGlobalEffect (mGeneratorData.mFreqGain);
			SetGlobalEffect (mGeneratorData.mLowpassCutoffFreq);
			SetGlobalEffect (mGeneratorData.mLowpassResonance);
			SetGlobalEffect (mGeneratorData.mHighpassCutoffFreq);
			SetGlobalEffect (mGeneratorData.mHighpassResonance);
			SetGlobalEffect (mGeneratorData.mEchoDelay);
			SetGlobalEffect (mGeneratorData.mEchoDecay);
			SetGlobalEffect (mGeneratorData.mEchoDry);
			SetGlobalEffect (mGeneratorData.mEchoWet);
			SetGlobalEffect (mGeneratorData.mNumEchoChannels);
			SetGlobalEffect (mGeneratorData.mReverb);
			SetGlobalEffect (mGeneratorData.mRoomSize);
			SetGlobalEffect (mGeneratorData.mReverbDecay);
		}
	}
}