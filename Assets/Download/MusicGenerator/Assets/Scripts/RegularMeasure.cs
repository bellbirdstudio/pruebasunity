﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	/// A regular, non-repeating measure.
	public class RegularMeasure : Measure
	{
		/// Plays through the next step in the measure.
		public override void PlayMeasure (InstrumentSet set, Action CheckKeyChange = null, Action SetThemeRepeat = null, Action GenerateNewProgression = null)
		{
			if (set.mMusicGenerator == null)
				return;

			set.UpdateTempo ();
			set.mSixteenthStepTimer -= Time.deltaTime;
			if (set.mSixteenthStepTimer <= 0 && set.SixteenthStepsTaken < set.mTimeSignature.mStepsPerMeasure)
			{
				set.mMusicGenerator.BarlineColorSet.Invoke (set.SixteenthStepsTaken, false);

				if (set.SixteenthStepsTaken % (int) set.mData.mProgressionRate == set.mTimeSignature.Whole)
				{
					set.ProgressionStepsTaken += 1;
					set.ProgressionStepsTaken = set.ProgressionStepsTaken % set.mMusicGenerator.ChordProgression.Length;
					if (CheckKeyChange != null)
						CheckKeyChange ();
				}
				if (set.SixteenthStepsTaken % set.mTimeSignature.Half == 0)
					TakeStep (set, eTimestep.eighth, set.ProgressionStepsTaken);
				if (set.SixteenthStepsTaken % set.mTimeSignature.Quarter == 0)
					TakeStep (set, eTimestep.quarter, set.ProgressionStepsTaken);
				if (set.SixteenthStepsTaken % set.mTimeSignature.Eighth == 0)
					TakeStep (set, eTimestep.half, set.ProgressionStepsTaken);
				if (set.SixteenthStepsTaken % set.mTimeSignature.Sixteenth == 0)
				{
					TakeStep (set, eTimestep.whole, set.ProgressionStepsTaken);
					set.mMeasureStartTimer = 0.0f;
				}

				TakeStep (set, eTimestep.sixteenth, set.ProgressionStepsTaken);

				set.mSixteenthStepTimer = set.mBeatLength;
				set.SixteenthStepsTaken += 1;
			}
			else if (set.SixteenthStepsTaken == set.mTimeSignature.mStepsPerMeasure)
			{
				set.mMeasureStartTimer += Time.deltaTime;

				if (set.mMeasureStartTimer > set.mBeatLength) //We don't actually want to reset until the next beat.
				{
					GenerateNewProgression ();
					ResetMeasure (set, SetThemeRepeat);
				}
			}
		}

		/// Plays the next step in the measure.
		public override void TakeStep (InstrumentSet set, eTimestep timeStepIN, int stepsTaken = 0)
		{
			for (int instIndex = 0; instIndex < set.mInstruments.Count; instIndex++)
			{
				if (set.mInstruments[instIndex].mData.Group >= set.mMusicGenerator.mGeneratorData.mGroupOdds.Count || set.mData.mProgressionRate < 0)
					return;

				Instrument instrument = set.mInstruments[instIndex];
				bool groupIsPlaying = set.mMusicGenerator.mGroupIsPlaying[(int) instrument.mData.Group];

				if (instrument.mData.mTimeStep == timeStepIN && groupIsPlaying && !instrument.mData.mIsMuted)
					PlayNotes (set, instrument, stepsTaken, instIndex);
			}
		}

		/// Exits a non-repeating measure, resetting values to be able to play the next:
		public override void ResetMeasure (InstrumentSet set, Action SetThemeRepeat = null, bool hardReset = false, bool isRepeating = true)
		{
			ResetRegularMeasure (set, SetThemeRepeat, hardReset, isRepeating);
		}

		/// Plays the notes for this timestep
		private void PlayNotes (InstrumentSet set, Instrument instrument, int stepsTaken, int instIndex)
		{
			/// we want to fill this whether we play it or not:
			int progressionStep = set.mMusicGenerator.ChordProgression[stepsTaken];
			int[] clip = instrument.GetProgressionNotes (progressionStep);
			if (instrument.mData.StrumLength == 0.0f || instrument.mData.mSuccessionType != eSuccessionType.rhythm)
			{
				for (int j = 0; j < clip.Length; j++)
				{
					if (clip[j] != InstrumentSet.mUnplayed) //we ignore -1
					{
						int numSubInstruments = set.mMusicGenerator.AllClips[(int) instrument.InstrumentTypeIndex].Count;
						int instrumentSubIndex = UnityEngine.Random.Range (0, numSubInstruments);
						try
						{
							set.mMusicGenerator.PlayAudioClip (set, set.mMusicGenerator.AllClips[(int) instrument.InstrumentTypeIndex][instrumentSubIndex][clip[j]], instrument.mData.Volume, instIndex);
							set.mMusicGenerator.UIStaffNotePlayed.Invoke (clip[j], (int) instrument.mData.mStaffPlayerColor);
						}
						catch (ArgumentOutOfRangeException e)
						{
							throw new ArgumentOutOfRangeException (e.Message);
						}
					}
				}
			}
			else
				set.Strum (clip, instIndex);
		}
	}
}