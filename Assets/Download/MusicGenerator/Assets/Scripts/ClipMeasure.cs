﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	public class ClipMeasure : Measure
	{
		/// Plays the next steps in the measure.
		public override void PlayMeasure (InstrumentSet set, Action CheckKeyChange = null, Action SetThemeRepeat = null, Action GenerateNewProgression = null)
		{
			set.UpdateTempo ();

			if (set.mMusicGenerator == null)
				return;

			set.mSixteenthStepTimer -= Time.deltaTime;
			if (set.mSixteenthStepTimer <= 0 && set.SixteenthStepsTaken < set.mTimeSignature.mStepsPerMeasure)
			{
				if (set.SixteenthStepsTaken % (int) set.mData.mProgressionRate == 0)
					set.ProgressionStepsTaken += 1;
				if (set.ProgressionStepsTaken > InstrumentSet.mMaxFullstepsTaken - 1)
					set.ProgressionStepsTaken = -1;

				TakeStep (set, (int) eTimestep.sixteenth, set.SixteenthRepeatCount);
				set.SixteenthRepeatCount += 1;
				set.mSixteenthStepTimer = set.mBeatLength;

				set.SixteenthStepsTaken += 1;
			}
			else if (set.SixteenthStepsTaken == set.mTimeSignature.mStepsPerMeasure)
			{
				set.mMeasureStartTimer += Time.deltaTime;
				if (set.mMeasureStartTimer > set.mBeatLength)
				{
					bool hardReset = false;
					ResetMeasure (set, SetThemeRepeat, hardReset, true);
				}
			}
		}

		/// Exits our measure
		public override void ResetMeasure (InstrumentSet set, Action SetThemeRepeat = null, bool hardReset = false, bool isRepeating = true)
		{
			ResetRepeatMeasure (set, SetThemeRepeat, hardReset, isRepeating);
		}

		/// Takes a measure step.
		public override void TakeStep (InstrumentSet set, eTimestep timeStepIN, int value = 0)
		{
			for (int i = 0; i < set.mInstruments.Count; i++)
			{
				if (!set.mInstruments[i].mData.mIsMuted)
				{
					for (int j = 0; j < set.mInstruments[i].mData.ChordSize; j++)
					{
						int note = set.mInstruments[i].mClipNotes[set.mRepeatCount][value][j];
						int instrumentSubIndex = UnityEngine.Random.Range (0, set.mMusicGenerator.AllClips[(int) set.mInstruments[i].InstrumentTypeIndex].Count);
						if (note != InstrumentSet.mUnplayed)
						{
							/// set percussion to 0
							if (set.mInstruments[i].mData.InstrumentType.Contains ("P_"))
								note = 0;

							set.mMusicGenerator.PlayAudioClip (set, set.mMusicGenerator.AllClips[(int) set.mInstruments[i].InstrumentTypeIndex][instrumentSubIndex][note],
								set.mInstruments[i].mData.Volume, i);
						}
					}
				}
			}
		}
	}
}