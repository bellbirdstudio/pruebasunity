using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	/// Instrument Class. Handles selecting notes to play and other instrument settings.
	[System.Serializable]
	public class Instrument
	{
		/// our data.
		[Tooltip ("The data for this instrument")]
		public InstrumentData mData = null;

		/// index of MusicGenerator.mInstruments()
		private int mInstrumentIndex = 0;
		public int InstrumentIndex { get { return mInstrumentIndex; } set { mInstrumentIndex = value < MusicGenerator.mMaxInstruments ? value : 0; } }

		/// how many notes in an octave.
		public const int mOctave = 12;

		/// scale steps in a seventh chord
		static readonly public int[] mSeventhChord = new int[] { 0, 2, 4, 6 };

		/// number of notes in a triad :P
		public const int mTriadCount = 3;

		/// just a base of 1.
		public const float mOddsOfPlayingMultiplierBase = 1;

		/// number of steps per measure (this is the max. when using 5/4 signature. it could be as low as 12.)
		public const int mStepsPerMeasure = 20;

		/// value for an 'unused' note 
		private const int mUnplayed = -1;

		//steps between notes. Used in scales:
		public const int mHalfStep = 1;
		public const int mFullStep = 2;
		public const int mFullPlusHalf = 3;
		//for exotic scales. Currently not implemented
		//static readonly private int mDoubleStep = 4; 

		///amount to adjust tritone chords/ notes.
		public const int mTritoneStep = 5;

		/// when created, we flag ourselves to generate a new theme/repeat, as this may
		/// have happened mid-measure.
		public bool mNeedsTheme { get; private set; }

		// our scales: TO NOTE: Melodic minor is both ascending and descending, which isn't super accurate for classical theory, generally. But, it was causing issue so
		// now just uses the scale in both ascend/descending melodies. It's on the wishlist, but is problematic for a few reasons.
		static readonly public int[] mMelodicMinor = new int[] { mFullStep, mHalfStep, mFullStep, mFullStep, mFullStep, mFullStep, mHalfStep };
		static readonly public int[] mNaturalMinor = new int[] { mFullStep, mHalfStep, mFullStep, mFullStep, mHalfStep, mFullStep, mFullStep };
		static readonly public int[] mMajorScale = new int[] { mFullStep, mFullStep, mHalfStep, mFullStep, mFullStep, mFullStep, mHalfStep };
		static readonly private int[] mHarmonicMinor = new int[] { mFullStep, mHalfStep, mFullStep, mFullStep, mHalfStep, mFullPlusHalf, mHalfStep };
		static readonly private int[] mHarmonicMajor = new int[] { mFullStep, mFullStep, mHalfStep, mFullStep, mHalfStep, mFullPlusHalf, mHalfStep };
		static readonly public int[][] mMusicScales = new int[][] { mMajorScale, mNaturalMinor, mMelodicMinor, mHarmonicMinor, mHarmonicMajor, mMelodicMinor };

		/// length of our scales. Currently all 7 notes.
		public const int mScaleLength = 7;

		/// list of our chord offsets to use in a pattern
		public int[][] mPatternNoteOffset = new int[mStepsPerMeasure][];

		/// list of our octave offsets to use in a pattern
		public int[][] mPatternOctaveOffset = new int[mStepsPerMeasure][];

		/// which steps we're using.
		public int mPatternstepsTaken { get; private set; }

		/// Resets the number of pattern steps taken (this is handled internally by the instrument set and shouldn't need to be manually changed.)
		public void ResetPatternStepsTaken () { mPatternstepsTaken = 0; }

		//Variables used by the music generator
		public int[][] mRepeatingNotes = new int[64][]; /// list of notes played last measure
		public int[][] mThemeNotes = new int[64][]; /// notes of saved theme measure
		public int[][][] mClipNotes = new int[4][][]; ///measure; timestep ; note.  Info for playing a clip.

		/// Reference to our music generator
		public MusicGenerator mMusicGenerator { get; private set; }

		/// whether we're repeating the pattern this iteration
		public bool mbAreRepeatingPattern { get; private set; }

		/// whether we're setting the pattern this iteration
		public bool mbAreSettingPattern { get; private set; }

		/// which index of the pattern we're playing this iteration
		public int mCurrentPatternStep { get; private set; }

		/// list of current pattern notes for this step:
		public int[] mCurrentPatternNotes = new int[4] {-1, -1, -1, -1 };

		/// list of current pattern octave offsets for this step:
		public int[] mCurrentPatternOctave = new int[4] { 0, 0, 0, 0 };

		/// what our current step of the chord progression is.
		public int mCurrentProgressionStep = 0;

		/// pattern for our arpeggio.
		public int[] mArpeggioPattern = new int[4] { 0, 2, 4, -1 };
		private int[] mTriadPattern = new int[3] { 0, 2, 4 };
		private int[] mSeventhPattern = new int[4] { 0, 2, 4, 6 };

		public static System.Random sRandom = new System.Random ();
		/// Generates an arpeggio pattern for this measure:
		public void GenerateArpeggio ()
		{
			int[] notes = mData.ChordSize == mTriadCount ? mTriadPattern : mSeventhPattern;
			for (int i = 0; i < notes.Length; i++)
			{
				int rand = sRandom.Next (i + 1);
				int temp = notes[i];
				notes[i] = notes[rand];
				mArpeggioPattern[i] = notes[rand];
				notes[rand] = temp;
				mArpeggioPattern[rand] = temp;
			}
			if (mData.ChordSize == mTriadCount)
				mArpeggioPattern[3] = -1;
		}

		[Tooltip ("Index for the type of instrument. beware setting manually")]
		private int mInstrumentTypeIndex = 0;
		public int InstrumentTypeIndex { get { return mInstrumentTypeIndex; } set { mInstrumentTypeIndex = value < MusicGenerator.Instance.AllClips.Count ? value : 0; } }

		/// our progression notes to be played
		private int[] mProgressionNotes = new int[] {-1, -1, -1, -1 };

		/// our note generators. TODO: inject these rather than hard-code.
		public NoteGenerator[] mNoteGenerators = new NoteGenerator[] { new NoteGenerator_Melody (), new NoteGenerator_Rhythm (), new NoteGenerator_Lead () };

		/// Instrument initialization
		public void Init (int index)
		{
			mNeedsTheme = true;
			mbAreRepeatingPattern = false;
			mbAreSettingPattern = false;
			mCurrentPatternStep = 0;
			mData = new InstrumentData ();
			InstrumentIndex = index;
			mPatternstepsTaken = 0;

			mMusicGenerator = MusicGenerator.Instance;
			LoadClipNotes ();
			InitRepeatingAndThemeNotes ();
			ClearClipNotes ();
			InitPatternNotes ();
			SetupNoteGenerators ();
		}

		/// Returns a list of ints coresponding to the notes to play
		public int[] GetProgressionNotes (int progressionStep)
		{
			SetupNoteGeneration (progressionStep);
			SelectNotes ();
			CheckValidity ();
			SetRepeatNotes ();
			SetMultiplier ();
			return mProgressionNotes;
		}

		/// Resets the instrument.
		public void ResetInstrument ()
		{
			ClearThemeNotes ();
			ClearPlayedLeadNotes ();
			ClearPatternNotes ();
		}

		/// Clears our played lead notes.
		public void ClearPlayedLeadNotes ()
		{
			mNoteGenerators[(int) eSuccessionType.lead].ClearNotes ();
		}

		/// Sets the pattern variables. Mostly for readability in other functions :\
		private void SetupNoteGeneration (int progressionStep)
		{
			InstrumentSet set = mMusicGenerator.mInstrumentSet;
			int invProgRate = set.GetInverseProgressionRate ((int) mData.mTimeStep);
			int progRate = set.GetProgressionRate ((int) mData.mTimeStep);

			// chord progressions are set in their sensible way: I-IV-V for example starting on 1. 
			// it's easier to leave like that as it's readable (from a music perspective, anyhow) and adjust here, rather than 0 based:
			mCurrentProgressionStep = progressionStep - ((progressionStep < 0) ? -1 : 1);

			mPatternstepsTaken = (int) (set.SixteenthStepsTaken / invProgRate);
			mCurrentPatternStep = mPatternstepsTaken % mData.PatternLength;

			mbAreRepeatingPattern = (mPatternstepsTaken >= mData.PatternLength && mPatternstepsTaken < progRate - mData.PatternRelease);
			mbAreSettingPattern = (mPatternstepsTaken < mData.PatternLength);

			if (mCurrentPatternStep < mPatternNoteOffset.Length - 1)
			{
				mCurrentPatternNotes = mPatternNoteOffset[(int) mCurrentPatternStep];
				mCurrentPatternOctave = mPatternOctaveOffset[(int) mCurrentPatternStep];
			}
		}

		/// Sets up our note generators.
		private void SetupNoteGenerators ()
		{
			NoteGenerator.Fallback melodicFallback = x => mNoteGenerators[(int) eSuccessionType.rhythm].GenerateNotes ();
			NoteGenerator.Fallback leadFallback = x => mNoteGenerators[(int) eSuccessionType.melody].GenerateNotes ();
			mNoteGenerators[(int) eSuccessionType.lead].Init (this, leadFallback);
			mNoteGenerators[(int) eSuccessionType.melody].Init (this, melodicFallback);
			mNoteGenerators[(int) eSuccessionType.rhythm].Init (this, null);
		}

		/// Sets our notes to repeat.
		private void SetRepeatNotes ()
		{
			InstrumentSet set = mMusicGenerator.mInstrumentSet;
			int count = set.SixteenthStepsTaken + (set.mRepeatCount * set.mTimeSignature.mStepsPerMeasure);
			for (int i = 0; i < mProgressionNotes.Length; i++)
				mRepeatingNotes[count][i] = mProgressionNotes[i];
		}

		/// Sets our array of notes to play, based on rhythm/leading and other variables:
		private void SelectNotes ()
		{
			mProgressionNotes = mNoteGenerators[(int) mData.mSuccessionType].GenerateNotes ();
		}

		/// Checks for out of range notes in our list and forces it back within range.
		private void CheckValidity ()
		{
			if (mProgressionNotes.Length != mSeventhChord.Length)
				throw new Exception ("We haven't fully filled our note array. Something has gone wrong.");

			for (int i = 0; i < mProgressionNotes.Length; i++)
			{
				int note = mProgressionNotes[i];
				int clipArraySize = MusicGenerator.mMaxInstrumentNotes;

				if (note == mUnplayed || (note < clipArraySize && note >= mUnplayed))
					continue;

				if (note < 0)
					note = MusicHelpers.SafeLoop (note, mOctave);
				else if (note >= clipArraySize)
				{
					note = MusicHelpers.SafeLoop (note, mOctave);
					note += (mData.mUsePattern && mbAreRepeatingPattern) ? mCurrentPatternOctave[i] * mOctave : 2 * mOctave;
				}

				/// if somehow this is still out of range, we've utterly broken things...
				if (note < 0 || note > clipArraySize)
				{
					Debug.Log ("something's gone wrong note is out of range.");
					note = 0;
				}

				mProgressionNotes[i] = note;
			}
		}

		/// Sets the theme notes from the repeating list.
		public void SetThemeNotes ()
		{
			mNeedsTheme = false;
			for (int x = 0; x < mRepeatingNotes.Length; x++)
				for (int y = 0; y < mRepeatingNotes[x].Length; y++)
					mThemeNotes[x][y] = mRepeatingNotes[x][y];
		}

		/// sets our multiplier for the next played note:
		private void SetMultiplier ()
		{
			mData.OddsOfPlayingMultiplier = mOddsOfPlayingMultiplierBase;
			for (int i = 0; i < mProgressionNotes.Length; i++)
			{
				if (mProgressionNotes[i] != mUnplayed)
					mData.OddsOfPlayingMultiplier = mData.OddsOfPlayingMultiplierMax;
			}
		}

		/// Just initializes our clip notes to unplayed.
		private void LoadClipNotes ()
		{
			int numMeasures = 4;
			for (int x = 0; x < numMeasures; x++)
			{
				mClipNotes[x] = new int[mStepsPerMeasure][];
				for (int y = 0; y < mClipNotes[x].Length; y++)
				{
					mClipNotes[x][y] = new int[MusicGenerator.mMaxInstrumentNotes];
					for (int z = 0; z < mClipNotes[x][y].Length; z++)
						mClipNotes[x][y][z] = mUnplayed;
				}

			}
		}

		/// Initializes our repeat/theme notes arrays.
		private void InitRepeatingAndThemeNotes ()
		{
			for (int x = 0; x < mRepeatingNotes.Length; x++)
			{
				mRepeatingNotes[x] = new int[mSeventhChord.Length];
				mThemeNotes[x] = new int[mSeventhChord.Length];
				for (int y = 0; y < mSeventhChord.Length; y++)
				{
					mRepeatingNotes[x][y] = -1;
					mThemeNotes[x][y] = -1;
				}
			}
		}

		/// Clears the repeating note array.
		public void ClearRepeatingNotes ()
		{
			for (int x = 0; x < mRepeatingNotes.Length; x++)
				for (int y = 0; y < mSeventhChord.Length; y++)
					mRepeatingNotes[x][y] = -1;
		}

		/// Clears the theme note array.
		public void ClearThemeNotes ()
		{
			mNeedsTheme = true;
			for (int x = 0; x < mThemeNotes.Length; x++)
				for (int y = 0; y < mThemeNotes[x].Length; y++)
					mThemeNotes[x][y] = -1;
		}

		/// Clears the pattern notes.
		public void ClearPatternNotes ()
		{
			for (int i = 0; i < mStepsPerMeasure; i++)
			{
				for (int j = 0; j < mSeventhChord.Length; j++)
				{
					mPatternNoteOffset[i][j] = mUnplayed;
					mPatternOctaveOffset[i][j] = 0;
				}
			}
		}

		/// Initializes our pattern notes.
		private void InitPatternNotes ()
		{
			for (int i = 0; i < mStepsPerMeasure; i++)
			{
				mPatternOctaveOffset[i] = new int[mSeventhChord.Length];
				mPatternNoteOffset[i] = new int[mSeventhChord.Length];
				for (int j = 0; j < mSeventhChord.Length; j++)
				{
					mPatternNoteOffset[i][j] = mUnplayed;
					mPatternOctaveOffset[i][j] = 0;
				}
			}
		}

		// -----------------------------------------------------------
		// clip management: This is only used the the UI version of the player to set clip notes:
		// should probably be moved out of here, or stored elsewhere.
		// -----------------------------------------------------------

		/// Adds the clip note.
		public bool AddClipNote (int timestep, int note, int measure = 0)
		{
			for (int i = 0; i < mClipNotes[measure][timestep].Length; i++)
			{
				if (mClipNotes[measure][timestep][i] == mUnplayed)
				{
					mClipNotes[measure][timestep][i] = note;
					return true;
				}
			}
			return false;
		}

		/// Removes the clip note.
		public bool RemoveClipNote (int timestep, int note, int measure = 0)
		{
			for (int i = 0; i < mClipNotes[measure][timestep].Length; i++)
			{
				if (mClipNotes[measure][timestep][i] == note)
				{
					mClipNotes[measure][timestep][i] = mUnplayed;
					return true;
				}
			}
			return false;
		}

		/// Clears the clip notes.
		public void ClearClipNotes ()
		{
			for (int x = 0; x < mClipNotes.Length; x++)
				for (int i = 0; i < mClipNotes[x].Length; i++)
					for (int j = 0; j < mClipNotes[x][i].Length; j++)
						mClipNotes[x][i][j] = mUnplayed;
		}

		/////////////////////////////////////////
		/// Save / Load functions.
		/////////////////////////////////////////

		/// loads and sets values from save file.
		public void LoadInstrument (InstrumentData data)
		{
			mData = data;
			string stringIndex = InstrumentIndex.ToString ();
			mMusicGenerator.mMixer.SetFloat ("Volume" + stringIndex, data.AudioSourceVolume);
			mMusicGenerator.mMixer.SetFloat ("Reverb" + stringIndex, mData.Reverb);
			mMusicGenerator.mMixer.SetFloat ("RoomSize" + stringIndex, mData.RoomSize);
			mMusicGenerator.mMixer.SetFloat ("Chorus" + stringIndex, mData.Chorus);
			mMusicGenerator.mMixer.SetFloat ("Flange" + stringIndex, mData.Flanger);
			mMusicGenerator.mMixer.SetFloat ("Distortion" + stringIndex, mData.Distortion);
			mMusicGenerator.mMixer.SetFloat ("Echo" + stringIndex, mData.Echo);
			mMusicGenerator.mMixer.SetFloat ("EchoDelay" + stringIndex, mData.EchoDelay);
			mMusicGenerator.mMixer.SetFloat ("EchoDecay" + stringIndex, mData.EchoDecay);
		}
	}
}