﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	public abstract class Measure : MonoBehaviour
	{
		/// Plays a step through measure.
		public abstract void PlayMeasure (InstrumentSet set, Action CheckKeyChange = null, Action SetThemeRepeat = null, Action GenerateNewProgression = null);
		
		/// Resets the measure
		public abstract void ResetMeasure (InstrumentSet set, Action SetThemeRepeat = null, bool hardReset = false, bool isRepeating = true);
		
		/// Takes a single measure step.
		public abstract void TakeStep (InstrumentSet set, eTimestep timeStepIN, int stepsTaken = 0);

		/// Resets a repeating measure.
		protected void ResetRepeatMeasure (InstrumentSet set, Action SetThemeRepeat = null, bool hardReset = false, bool isRepeating = true)
		{
			set.mMusicGenerator.RepeatedMeasureExited.Invoke (set.mMusicGenerator.mState);

			set.mRepeatCount += 1;

			set.mMeasureStartTimer = 0.0f;
			set.mSixteenthStepTimer = 0.0f;
			set.ResetMultipliers ();
			set.SixteenthStepsTaken = 0;

			if (!isRepeating)
				return;

			//if we've repeated all the measures set to repeat in their entirety, reset the step counts.
			if (set.mRepeatCount >= set.mData.RepeatMeasuresNum * 2 || set.mMusicGenerator.OnUIPlayerIsEditing () || hardReset)
			{
				set.mRepeatCount = 0;
				set.SixteenthRepeatCount = 0;
				for (int i = 0; i < set.mInstruments.Count; i++)
					set.mInstruments[i].ClearRepeatingNotes ();

				if (set.mMusicGenerator.mState > eGeneratorState.stopped && set.mMusicGenerator.mState < eGeneratorState.editorInitializing)
					set.mMusicGenerator.SetState (eGeneratorState.playing);
			}
		}

		/// Resets a regular measure.
		protected void ResetRegularMeasure (InstrumentSet set, Action SetThemeRepeat = null, bool hardReset = false, bool isRepeating = true)
		{
			if (set.mMusicGenerator == null)
				throw new ArgumentNullException ("music generator does not exist. Please ensure a game object with this class exists");

			set.mRepeatCount += 1;

			if (SetThemeRepeat != null)
				SetThemeRepeat ();

			for (int i = 0; i < set.mInstruments.Count; i++)
			{
				set.mInstruments[i].ClearPatternNotes ();
				set.mInstruments[i].ResetPatternStepsTaken ();
				set.mInstruments[i].ClearPlayedLeadNotes ();
				set.mInstruments[i].GenerateArpeggio();
			}
			set.SixteenthStepsTaken = 0;

			//select groups:
			set.SelectGroups ();

			if (set.ProgressionStepsTaken >= InstrumentSet.mMaxFullstepsTaken - 1)
				set.ProgressionStepsTaken = -1;

			if (set.mMusicGenerator.mGeneratorData.mThemeRepeatOptions == eThemeRepeatOptions.eNone)
			{
				set.mRepeatCount = 0;
				for (int i = 0; i < set.mInstruments.Count; i++)
					set.mInstruments[i].ClearRepeatingNotes ();
			}

			set.mMeasureStartTimer = 0.0f;
			set.mSixteenthStepTimer = 0.0f;

			set.ResetMultipliers ();
		}
	}
}