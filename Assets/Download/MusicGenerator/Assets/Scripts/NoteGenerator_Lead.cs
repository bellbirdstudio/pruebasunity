﻿using System.Collections.Generic;
using System.Collections;

namespace ProcGenMusic
{
	public class NoteGenerator_Lead : NoteGenerator
	{
		/// list of melodic notes we've already played (for determining ascend/descend)
		public List<int> mPlayedMelodicNotes = new List<int> ();

		/// for lead influence
		public static readonly int mDescendingInfluence = -1;

		/// for lead influence 
		public static readonly int mAscendingInfluence = 1;

		/// Clears the stored melodic notes that have played.
		public override void ClearNotes () { mPlayedMelodicNotes.Clear (); }

		/// Generates the next notes for this lead instrument.
		public override int[] GenerateNotes ()
		{
			if (IsPercussion ())
				return GetPercussionNotes ();
			else if (UnityEngine.Random.Range (0, 100) > mInstrument.mData.OddsOfUsingChordNotes)
			{
				if (UnityEngine.Random.Range (0, 100) < mInstrument.mData.OddsOfPlaying * mInstrument.mData.OddsOfPlayingMultiplier)
				{
					int nextNote = GetRawLeadNoteIndex ();
					if (nextNote < 0) // just a safety check to keep from going under low C.
						nextNote *= -1;

					/// here we find the shortest rhythm step and make sure we're not playing something dischordant if it may be playing as well.
					int shortestTimestep = (int) mMusicGenerator.GetShortestRhythmTimestep ();
					shortestTimestep = mMusicGenerator.mInstrumentSet.GetInverseProgressionRate (shortestTimestep);
					if (shortestTimestep == 1 || mMusicGenerator.mInstrumentSet.SixteenthStepsTaken % shortestTimestep == 0)
					{
						if (IsAvoidNote (nextNote))
							nextNote = FixAvoidNote (nextNote);
					}

					mPlayedMelodicNotes.Add (nextNote);
					int note = AdjustRawLeadIndex (nextNote);
					AddSingleNote (note);
				}
				else
					AddEmptyNotes ();

				return mNotes;
			}
			else
				return mFallback ();
		}

		/// Gets the next melodic note.
		private int GetRawLeadNoteIndex ()
		{
			int[] scale = Instrument.mMusicScales[(int) mMusicGenerator.mGeneratorData.mScale];
			int noteOUT = UnityEngine.Random.Range (0, (scale.Length - 1)) + (GetOctave ());

			if (mPlayedMelodicNotes.Count == 1)
			{
				noteOUT = mPlayedMelodicNotes[mPlayedMelodicNotes.Count - 1] +
					(UnityEngine.Random.Range (1, (int) mInstrument.mData.LeadMaxSteps) * mInstrument.mData.LeadInfluence);
			}
			if (mPlayedMelodicNotes.Count > 1)
			{
				int ultimateNote = mPlayedMelodicNotes[mPlayedMelodicNotes.Count - 1];
				noteOUT = UnityEngine.Random.Range (ultimateNote + mInstrument.mData.LeadInfluence, ultimateNote + ((int) mInstrument.mData.LeadMaxSteps * mInstrument.mData.LeadInfluence));
			}

			/// here, we try to stay within range, and adjust the ascend/ descend influence accordingly:
			int maxOctaves = 3;
			if (noteOUT + (int) mMusicGenerator.mGeneratorData.mKey + mInstrument.mCurrentPatternStep > (Instrument.mScaleLength * maxOctaves) - 2)
			{
				int progressionStep = mInstrument.mCurrentProgressionStep < 0 ? mInstrument.mCurrentProgressionStep * -1 : mInstrument.mCurrentProgressionStep;

				noteOUT = (Instrument.mScaleLength * maxOctaves) - 3 - (int) mMusicGenerator.mGeneratorData.mKey - progressionStep;
				mInstrument.mData.LeadInfluence = mDescendingInfluence;
			}
			else if (noteOUT < 0)
			{
				noteOUT = UnityEngine.Random.Range (1, (int) mInstrument.mData.LeadMaxSteps);
				mInstrument.mData.LeadInfluence = mAscendingInfluence;
			}
			else if (UnityEngine.Random.Range (0, 100) > mInstrument.mData.AscendDescendInfluence)
				mInstrument.mData.LeadInfluence *= -1;

			return noteOUT;
		}

		///Returns true if this lead note is a half step above a chord note:
		private bool IsAvoidNote (int noteIN)
		{
			int note = MusicHelpers.SafeLoop (noteIN - 1, Instrument.mScaleLength);
			int progressionstep = mInstrument.mCurrentProgressionStep < 0 ? mInstrument.mCurrentProgressionStep * -1 : mInstrument.mCurrentProgressionStep;
			int tritone = mInstrument.mCurrentProgressionStep < 0 ? -Instrument.mTritoneStep : 0;
			int adjustedNote = MusicHelpers.SafeLoop (note + tritone, Instrument.mScaleLength);
			int scaleNote = MusicHelpers.SafeLoop (adjustedNote + (int) mMusicGenerator.mGeneratorData.mMode + progressionstep, Instrument.mScaleLength);
			int[] scale = Instrument.mMusicScales[(int) mMusicGenerator.mGeneratorData.mScale];
			bool isHalfStep = scale[scaleNote] == Instrument.mHalfStep;

			bool isAboveChordNode = (adjustedNote == Instrument.mSeventhChord[0] ||
				adjustedNote == Instrument.mSeventhChord[1] ||
				adjustedNote == Instrument.mSeventhChord[2] ||
				(adjustedNote == Instrument.mSeventhChord[3] && mInstrument.mData.ChordSize == Instrument.mSeventhChord.Length));

			bool isSeventh = (mInstrument.mData.ChordSize != Instrument.mSeventhChord.Length && note == 6);
			if ((isHalfStep && isAboveChordNode) || isSeventh)
				return true;
			return false;
		}

		/// Fixes an avoid note to (hopefully) not be dischordant:
		private int FixAvoidNote (int nextNote)
		{
			int adjustedNote = nextNote + mInstrument.mData.LeadInfluence;
			int maxOctaves = 3;
			if ((adjustedNote + (int) mMusicGenerator.mGeneratorData.mKey + mInstrument.mCurrentPatternStep > (Instrument.mScaleLength * maxOctaves) - 3) ||
				adjustedNote < 0)
			{
				mInstrument.mData.LeadInfluence *= -1;
				adjustedNote = nextNote + mInstrument.mData.LeadInfluence;
			}
			bool isAvoidNote = true;
			int maxAttempts = Instrument.mScaleLength;
			for (int i = 1; i < maxAttempts && isAvoidNote; i++)
			{
				adjustedNote = nextNote + (i * mInstrument.mData.LeadInfluence);
				if (!IsAvoidNote (adjustedNote))
				{
					nextNote = adjustedNote;
					isAvoidNote = false;
				}
			}
			return adjustedNote;
		}

		/// steps the note through the scale, adjusted for mode, key, progression step to find th
		/// actual note index instead of our raw steps.
		private int AdjustRawLeadIndex (int noteIN)
		{
			int note = 0;
			int progressionstep = (mInstrument.mCurrentProgressionStep < 0) ? mInstrument.mCurrentProgressionStep * -1 : mInstrument.mCurrentProgressionStep;

			for (int j = 0; j < noteIN + progressionstep; j++)
			{
				int index = j + (int) mMusicGenerator.mGeneratorData.mMode;
				index = index % Instrument.mScaleLength;
				note += Instrument.mMusicScales[(int) mMusicGenerator.mGeneratorData.mScale][index];
			}
			note += (int) mMusicGenerator.mGeneratorData.mKey;
			return note;
		}
	}
}