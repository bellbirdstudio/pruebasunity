﻿using System.Collections.Generic;
using System.Collections;
using System.IO;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	[Serializable]
	public class InstrumentSetData
	{
		/// version number of this save.
		[SerializeField]
		private float mVersion = 0.0f;

		[Tooltip ("our time signature.")]
		public eTimeSignature mTimeSignature = eTimeSignature.FourFour;

		[Tooltip ("how quickly we step through our chord progression")]
		public eProgressionRate mProgressionRate = eProgressionRate.eight;

		[Tooltip ("current tempo")]
		[Range (1, 350)]
		[SerializeField]
		private float mTempo = 100.0f;
		public float Tempo { get { return mTempo; } set { mTempo = Mathf.Clamp (value, InstrumentSet.mMinTempo, InstrumentSet.mMaxTempo); } }

		[Tooltip ("number measure we'll repeat, if we're repeating measures")]
		[Range (1, 4)]
		[SerializeField]
		private int mRepeatMeasuresNum = 1;
		public int RepeatMeasuresNum { get { return mRepeatMeasuresNum; } set { mRepeatMeasuresNum = Mathf.Clamp (value, 1, 4); } }

		public static void SaveData (string pathIN, InstrumentSetData data)
		{
			data.mVersion = MusicGenerator.Version;
			pathIN = pathIN + "/InstrumentSetData.txt";
			string save = JsonUtility.ToJson (data);
			File.WriteAllText (pathIN, save);
		}

		public static InstrumentSetData LoadData (string pathIN)
		{
			string data = "";
			string instrumentSetDataPath = pathIN + "/InstrumentSetData.txt";
			if (File.Exists (instrumentSetDataPath))
				data = File.ReadAllText (instrumentSetDataPath);

			InstrumentSetData saveOUT = JsonUtility.FromJson<InstrumentSetData> (data);
			if (saveOUT == null || saveOUT.mVersion != MusicGenerator.Version)
				return UpdateVersion (pathIN, saveOUT);

			return saveOUT;
		}

		private static InstrumentSetData UpdateVersion (string pathIN, InstrumentSetData save)
		{
			if (save == null || save.mVersion == 0.0f)
			{
				string generatorPath = pathIN + "/generator.txt";
				/// we need to grab these from the generatorSave as the variables belonged to that in the last version
				if (File.Exists (generatorPath))
				{
					GeneratorSave generatorSave = JsonUtility.FromJson<GeneratorSave> (File.ReadAllText (generatorPath));
					save = new InstrumentSetData ();
					save.Tempo = generatorSave.mTempo;
					save.RepeatMeasuresNum = generatorSave.mRepeatMeasuresNum;
					save.mProgressionRate = (eProgressionRate) generatorSave.mProgressionRate;
					save.mTimeSignature = generatorSave.mTimeSignature;
				}
			}
			InstrumentSetData.SaveData(pathIN, save);
			return save;
		}
	}
}