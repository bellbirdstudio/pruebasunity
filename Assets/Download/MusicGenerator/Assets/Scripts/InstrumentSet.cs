﻿using System.Collections.Generic;
using System.Collections;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	/// The set of instruments for a configuration. Handles the timing, playing, repeating and other settings for its instruments.
	/// For normal uses of the generator, you should not need to call any of the public functions in here, as they're handled by the
	/// MusicGenerator or the SingleClip logic.
	[Serializable]
	public class InstrumentSet : MonoBehaviour
	{
		/// anything less than this isn't a functional tempo. Edit at your own risk.
		public const float mMinTempo = 1.0f;

		/// anything greater than this is likely to cause problems. Edit at your own risk.
		public const float mMaxTempo = 350.0f;

		/// max number of steps per progression. Currently only support 4
		static readonly public int mMaxFullstepsTaken = 4;

		private int mSixteenthRepeatCount = 0;
		public int SixteenthRepeatCount { get { return mSixteenthRepeatCount; } set { mSixteenthRepeatCount = Mathf.Clamp (value, 0, Instrument.mStepsPerMeasure * mData.RepeatMeasuresNum); } }

		/// number of 1/16 steps taken for current measure 
		private int mSixteenthStepsTaken = 0;
		public int SixteenthStepsTaken { get { return mSixteenthStepsTaken; } set { mSixteenthStepsTaken = Mathf.Clamp (value, 0, Instrument.mStepsPerMeasure); } }

		/// timer for single steps
		public float mSixteenthStepTimer = 0;

		/// length of measures. Used for timing.set on start
		public float mBeatLength { get; private set; }

		/// how many times we've repeated the measure.
		public int mRepeatCount = 0;

		/// resets the repeat count;
		public void ResetRepeatCount () { mRepeatCount = 0; }

		/// delay to balance out when we start a new measure
		public float mMeasureStartTimer = 0;

		/// how many steps in the chord progression have been taken
		private int mProgressionStepsTaken = -1;
		public int ProgressionStepsTaken { get { return mProgressionStepsTaken; } set { mProgressionStepsTaken = Mathf.Clamp (value, -1, InstrumentSet.mMaxFullstepsTaken); } }

		/// resets the progression steps taken.
		public void ResetProgressionSteps () { ProgressionStepsTaken = -1; }

		/// unplayed note
		public static readonly int mUnplayed = -1;

		/// if using linear dynamic style, this is our current level of groups that are playing.
		public int mCurrentGroupLevel { get; private set; }

		[Tooltip ("Our instrument set data.")]
		public InstrumentSetData mData = null;

		[Tooltip ("Reference to the music generator")]
		public MusicGenerator mMusicGenerator = null;

		[Tooltip ("Our time signature object.")]
		public TimeSignature mTimeSignature = new TimeSignature ();

		[Tooltip ("list of our current instruments")]
		public List<Instrument> mInstruments = new List<Instrument> ();

		void Awake ()
		{
			mBeatLength = 0;
			mCurrentGroupLevel = 0;
			mTimeSignature.Init ();
		}

		//Initializes music set.
		public void Init ()
		{
			mMusicGenerator = MusicGenerator.Instance;
		}

		/// Loads the instrument set data.
		public void LoadData (InstrumentSetData data)
		{
			mData = data;
			mTimeSignature.SetTimeSignature (data.mTimeSignature);
			mData.RepeatMeasuresNum = 1;
			UpdateTempo ();
		}

		/// Sets the time signature data:
		public void SetTimeSignature (eTimeSignature signature)
		{
			if (mData == null)
				return;
			mData.mTimeSignature = signature;
			mTimeSignature.SetTimeSignature (mData.mTimeSignature);
		}

		/// Resets the instrument set values:
		public void Reset ()
		{
			if (mMusicGenerator == null)
				return;

			mRepeatCount = 0;
			mCurrentGroupLevel = 0;

			ProgressionStepsTaken = -1;
			if (mMusicGenerator.mState == eGeneratorState.repeating)
				mMusicGenerator.SetState (eGeneratorState.playing);

			for (int i = 0; i < mInstruments.Count; i++)
				mInstruments[i].ResetInstrument ();
		}

		/// Gets the inverse progression rate.
		public int GetInverseProgressionRate (int valueIN)
		{
			valueIN = valueIN >= 0 && valueIN < mTimeSignature.mTimestepNumInverse.Length ? valueIN : 0;
			return mTimeSignature.mTimestepNumInverse[valueIN];
		}

		/// Returns the progression rate.
		public int GetProgressionRate (int valueIN)
		{
			valueIN = valueIN >= 0 && valueIN < mTimeSignature.mTimestepNum.Length ? valueIN : 0;
			return mTimeSignature.mTimestepNum[valueIN];
		}

		/// Sets the progression rate.
		public void SetProgressionRate (int valueIN)
		{
			valueIN = valueIN > 0 && valueIN <= mTimeSignature.mStepsPerMeasure ? valueIN : mTimeSignature.mStepsPerMeasure;
			mData.mProgressionRate = (eProgressionRate)GetInverseProgressionRate(valueIN);
		}

		/// Updates the tempo.
		public void UpdateTempo ()
		{
			int minute = 60;
			mBeatLength = minute / mData.Tempo; //beats per minute
		}

		/// strums a clip.
		public void Strum (int[] clipIN, int instIndex)
		{
			StartCoroutine (StrumClip (clipIN, instIndex));
		}

		//staggers the playClip() call:
		public IEnumerator StrumClip (int[] clipIN, int i)
		{
			if (mInstruments[i].mData.mSuccessionType == eSuccessionType.rhythm && !mInstruments[i].mData.mArpeggio)
				Array.Sort (clipIN);

			float variation = UnityEngine.Random.Range (0, mInstruments[i].mData.StrumVariation);
			for (int j = 0; j < clipIN.Length; j++)
			{
				if (clipIN[j] != mUnplayed)
				{
					int instrumentSubIndex = UnityEngine.Random.Range (0, mMusicGenerator.AllClips[(int) mInstruments[i].InstrumentTypeIndex].Count);
					mMusicGenerator.PlayAudioClip (this, mMusicGenerator.AllClips[(int) mInstruments[i].InstrumentTypeIndex][instrumentSubIndex][clipIN[j]], mInstruments[i].mData.Volume, i);
					mMusicGenerator.UIStaffNoteStrummed.Invoke (clipIN[j], (int) mInstruments[i].mData.mStaffPlayerColor);
					yield return new WaitForSeconds (mInstruments[i].mData.StrumLength + variation);
				}
			}
		}

		/// Selects which instrument groups will play next measure.
		public void SelectGroups ()
		{
			eGroupRate rate = mMusicGenerator.mGeneratorData.mGroupRate;
			if (rate == eGroupRate.eEndOfMeasure ||
				(rate == eGroupRate.eEndOfProgression && ProgressionStepsTaken >= mMaxFullstepsTaken - 1))
			{
				/// Either randomly choose which groups play or:
				if (mMusicGenerator.mGeneratorData.mDynamicStyle == eDynamicStyle.Random)
				{
					for (int i = 0; i < mMusicGenerator.mGroupIsPlaying.Count; i++)
						mMusicGenerator.mGroupIsPlaying[i] = (UnityEngine.Random.Range (0, 100.0f) < mMusicGenerator.mGeneratorData.mGroupOdds[i]);
				}
				else //we ascend / descend through our levels.
				{
					int ascend = 1;
					int descend = -1;
					int numGroup = mMusicGenerator.mGeneratorData.mGroupOdds.Count;

					int change = UnityEngine.Random.Range (0, 100) < 50 ? ascend : descend;
					int PotentialLevel = change + mCurrentGroupLevel;

					if (PotentialLevel < 0)
						PotentialLevel = mCurrentGroupLevel;
					if (PotentialLevel >= mMusicGenerator.mGeneratorData.mGroupOdds.Count)
						PotentialLevel = 0;

					//roll to see if we can change.
					if (UnityEngine.Random.Range (0, 100.0f) > mMusicGenerator.mGeneratorData.mGroupOdds[PotentialLevel])
						PotentialLevel = mCurrentGroupLevel;

					mCurrentGroupLevel = PotentialLevel;
					for (int i = 0; i < numGroup; i++)
						mMusicGenerator.mGroupIsPlaying[i] = i <= mCurrentGroupLevel;
				}
			}
		}

		/// Sets all multipliers back to their base.
		public void ResetMultipliers ()
		{
			for (int i = 0; i < mInstruments.Count; i++)
				mInstruments[i].mData.OddsOfPlayingMultiplier = Instrument.mOddsOfPlayingMultiplierBase;
		}

	}
}