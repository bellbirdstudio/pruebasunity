﻿using System.Collections.Generic;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System;
using UnityEngine;

namespace ProcGenMusic
{
	/// This class handles loading and saving of config files.
	public class MusicFileConfig : MonoBehaviour
	{
		/// saves a global configuration: instruments, instrument and global settings.
		public static void SaveConfiguration (string fileNameIN)
		{
			if (!Directory.Exists (Application.persistentDataPath + "/InstrumentSaves/" + fileNameIN))
				Directory.CreateDirectory (Application.persistentDataPath + "/InstrumentSaves/" + fileNameIN);
			string directory = GetDataDirectory (fileNameIN);

			MusicGeneratorData.SaveData (directory, MusicGenerator.Instance.mGeneratorData);
			InstrumentSetData.SaveData (directory, MusicGenerator.Instance.mInstrumentSet.mData);
			ChordProgressionData.SaveData (directory, MusicGenerator.Instance.mChordProgressions.mData);

			for (int i = 0; i < MusicGenerator.mMaxInstruments; i++)
			{
				string fileName = directory + "/instruments" + i + ".txt";
				if (i < MusicGenerator.Instance.mInstrumentSet.mInstruments.Count)
				{
					Instrument instrument = MusicGenerator.Instance.mInstrumentSet.mInstruments[i];
					InstrumentData.SaveData (fileName, instrument.mData);
				}
				else if (File.Exists (fileName))
				{
					/// If the user has deleted an instrument since the last save, 
					/// we need to delete that file.
					File.Delete (fileName);
				}
			}

			UnityEngine.Debug.Log (directory + " was successfully written to file");
		}

		/// Loads a global configuration: instruments, global settings, etc.
		public void LoadConfig (string folderIN)
		{
			MusicGenerator.Instance.ClearInstruments (MusicGenerator.Instance.mInstrumentSet);
			string resourceDirectory = GetDataDirectory (folderIN);

			LoadInstrumentSetData (resourceDirectory);
			LoadGeneratorData (resourceDirectory);
			LoadChordProgressionData (resourceDirectory);
			LoadInstrumentsData (resourceDirectory);
		}

		/// Returns the data directory for this filename.
		public static string GetDataDirectory (string folderIN)
		{
			string resourceDirectory = "";
			if (Directory.Exists (Application.persistentDataPath + "/InstrumentSaves/" + folderIN))
				resourceDirectory = Application.persistentDataPath + "/InstrumentSaves/" + folderIN;
			else if(Directory.Exists(Application.streamingAssetsPath + "/MusicGenerator/InstrumentSaves/" + folderIN))
				resourceDirectory = Application.streamingAssetsPath + "/MusicGenerator/InstrumentSaves/" + folderIN;
			else
				throw new NullReferenceException("Configuration for " + folderIN + " does not exist.");
			return resourceDirectory;
		}

		/// Loads the data for an instrument.
		private static void LoadInstrumentsData (string resourceDirectory)
		{
			InstrumentSet set = MusicGenerator.Instance.mInstrumentSet;
			for (int i = 0; i < MusicGenerator.mMaxInstruments; i++)
			{
				string path = resourceDirectory + "/instruments" + i.ToString () + ".txt";
				if (File.Exists (path))
				{
					InstrumentData instrumentData = InstrumentData.LoadData (path);
					MusicGenerator.Instance.AddInstrument (set);
					set.mInstruments[set.mInstruments.Count - 1].LoadInstrument (instrumentData);
					int index = MusicGenerator.Instance.LoadBaseClips (set.mInstruments[set.mInstruments.Count - 1].mData.InstrumentType);
					set.mInstruments[set.mInstruments.Count - 1].InstrumentTypeIndex = index;
				}
			}
		}

		/// Loads data for chord progressions
		private static void LoadChordProgressionData (string folderIN)
		{
			ChordProgressionData progressionData = ChordProgressionData.LoadData (folderIN);
			if (progressionData == null)
				throw new Exception (folderIN + " chord progression data failed to load");
			else
				MusicGenerator.Instance.mChordProgressions.LoadProgressionData (progressionData);
		}

		/// Loads data for the instrument set.
		private static void LoadInstrumentSetData (string folderIN)
		{
			InstrumentSetData setData = InstrumentSetData.LoadData (folderIN);
			MusicGenerator.Instance.mInstrumentSet.LoadData (setData);
		}

		/// Loads data for the Music Generator.
		private static void LoadGeneratorData (string folderIN)
		{
			MusicGeneratorData data = MusicGeneratorData.LoadData (folderIN);
			MusicGenerator.Instance.LoadGeneratorSave (data);
		}

		/// async Loads a global configuration: instruments, global settings, etc.
		public IEnumerator AsyncLoadConfig (string folderIN, System.Action<bool> callback)
		{

			MusicGenerator.Instance.ClearInstruments (MusicGenerator.Instance.mInstrumentSet);
			string resourceDirectory = GetDataDirectory (folderIN);

			LoadInstrumentSetData (resourceDirectory);
			yield return null;
			LoadGeneratorData (resourceDirectory);
			yield return null;
			LoadChordProgressionData (resourceDirectory);
			yield return null;
			bool finished = false;
			StartCoroutine (AsyncLoadInstrumentsData (resourceDirectory, ((x) => { finished = x; })));
			yield return new WaitUntil (() => finished);
			MusicGenerator.Instance.ResetPlayer();
			callback (true);
			yield return null;
		}

		// Async loads the instrument data.
		public IEnumerator AsyncLoadInstrumentsData (string resourceDirectory, System.Action<bool> callback)
		{
			///Load the instruments:
			InstrumentSet set = MusicGenerator.Instance.mInstrumentSet;
			for (int i = 0; i < MusicGenerator.mMaxInstruments; i++)
			{
				string path = resourceDirectory + "/instruments" + i.ToString () + ".txt";
				if (File.Exists (path))
				{
					InstrumentData instrumentData = InstrumentData.LoadData (path);
					if (instrumentData == null)
					{
						callback (true);
						yield break;
					}
					MusicGenerator.Instance.AddInstrument (set);
					yield return null;
					set.mInstruments[set.mInstruments.Count - 1].LoadInstrument (instrumentData);
					int index = 999;
					yield return null;
					StartCoroutine (MusicGenerator.Instance.AsyncLoadBaseClips (set.mInstruments[set.mInstruments.Count - 1].mData.InstrumentType, ((x) => { index = x; })));
					yield return new WaitUntil (() => index != 999);
					set.mInstruments[set.mInstruments.Count - 1].InstrumentTypeIndex = index;
				}
			}
			callback (true);
		}

		/// saves the tooltips.
		public static void SaveTooltips (string fileNameIN, TooltipSave save)
		{
			string fileName = Application.streamingAssetsPath + "/MusicGenerator/tooltips.txt";
			string serializedString = JsonUtility.ToJson (save);
			File.WriteAllText (fileName, serializedString);
			UnityEngine.Debug.Log ("tooltips saved");
		}

		/// loads the tooltips:
		public static TooltipSave LoadTooltips ()
		{
			string fileName = Application.streamingAssetsPath + "/MusicGenerator/tooltips.txt";
			string tooltipsString = File.ReadAllText (fileName);

			TooltipSave saveOUT = JsonUtility.FromJson<TooltipSave> (tooltipsString);

			if (saveOUT == null)
				throw new ArgumentNullException ("tooltip file was not sucessfully loaded");

			return saveOUT;
		}

		/// saves a clip configuration: 
		public static void SaveClipConfiguration (string fileNameIN, string serializedString)
		{
			string directory = Application.streamingAssetsPath + "/MusicGenerator/InstrumentClips/";
			if (!Directory.Exists (directory))
				Directory.CreateDirectory (directory);

			File.WriteAllText (directory + fileNameIN + ".txt", serializedString);
			UnityEngine.Debug.Log (fileNameIN + " saved");
		}

		/// Loads a clip configuration.
		public static ClipSave LoadClipConfigurations (string fileNameIN)
		{
			string clipSaveString = "";

			string streamingAssetsDir = Application.streamingAssetsPath + "/MusicGenerator/InstrumentClips/";
			string persistentDirectory = Application.persistentDataPath + "/MusicGenerator/InstrumentClips/";

			if (File.Exists (streamingAssetsDir + fileNameIN))
				clipSaveString = File.ReadAllText (streamingAssetsDir + fileNameIN);
			else
				clipSaveString = File.ReadAllText (persistentDirectory + fileNameIN);

			ClipSave clipSave = JsonUtility.FromJson<ClipSave> (clipSaveString);

			if (clipSave == null)
				throw new ArgumentNullException ("clipSave");

			return clipSave;
		}
	}
}