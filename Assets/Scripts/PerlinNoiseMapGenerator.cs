﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PerlinNoiseMapGenerator : MonoBehaviour {

    public int height = 256;
    public int width = 256;

    public int depth = 20;

    public float scale = 20f;

    Terrain terrain;

    private void Start()
    {
        terrain = GetComponent<Terrain>();
        terrain.terrainData = GenerarTerrainData(terrain.terrainData);

    }

        private TerrainData GenerarTerrainData(TerrainData terrainData)
    {
        terrainData.heightmapResolution = width; 

        terrainData.size = new Vector3(width, depth, height);

        terrainData.SetHeights(0, 0, GenerarHeights());


        return terrainData;
    }

    private float[,] GenerarHeights()
    {
        float[,] heights = new float[width, height];

        for (int i = 0; i < width; i++)
        {
            for(int j = 0; j< height;j++)
            {
                heights[i, j] = GenerarNoiseValue(i, j);
            }
        }
        return heights;
    }
    
    private float GenerarNoiseValue(int x, int y)
    {
        float xcoord = (float)(scale*width) - (float)x / width * scale;
        float ycoord = (float)(scale*height) - (float)y / height * scale;

        return Mathf.PerlinNoise(xcoord, ycoord);
    }

}
