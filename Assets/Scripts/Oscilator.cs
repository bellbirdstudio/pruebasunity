﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscilator : MonoBehaviour {

    public double frequency = 440;
    private float phase;
    private float increments;

    private double sample_frequency = 48000.0;

    public float gain = 0.1f;

    private void OnAudioFilterRead(float[] data, int channels)
    {
        increments = (float)(frequency * 2 * Mathf.PI / sample_frequency);

        for(var i = 0; i <data.Length; i = i + channels)
        {
            phase = phase + increments;
            data[i] = (float)(gain * Mathf.Sin(phase));
            if(channels == 2)
            {
                data[i + 1] = data[i];
                if (phase > 2 * Mathf.PI) phase = 0;
            }
        }
    }
}
